					<form role="search" method="get"  action="<?php echo home_url( '/' ); ?>">
 
						
						<div class="advance_search_field">
							
							<label for="auth"> Author(s) </label>


					
					<select id="" name="author_name" class="adv-select" >

						<?php
							  $paged=(get_query_var('paged')) ? get_query_var('paged') : 1;
							 
							  $query_args = array(
							    'post_type' => 'teacher',
							    'posts_per_page' => -1,
							    'paged' => $paged
							  );
							  $the_query = new WP_Query( $query_args );
						?>

						<?php 	
								if($the_query->have_posts()) : 
									while($the_query->have_posts()) : $the_query->the_post(); 
										global $post;
										$teacher_name= get_post_meta(get_the_ID(), '_cmb_name', true); 
						?>

							<option value="<?php echo $post->ID; ?>"> <?php echo $teacher_name; ?> </option>

						<?php endwhile; ?>

						<?php else: ?>
								<?php _e('Sorry, no posts matched your criteria.'); ?>
						<?php endif; ?>

					</select>
						
							<select id="" name="include_author_name" class="adv-select">
								<option value="word" selected="selected">Includes any of</option>
								<option value="allwords"> Includes all of</option>
							</select>


						<!-- 	<input type="text" name="author_name" id="author_name"> -->

						</div>

						<div class="advance_search_field">
						
							<label>Journal Name</label>

							<select id="" name="include_journal_name" class="adv-select">
								<option value="contains" selected="selected">Contains the phrase</option>
								<option value="word">Contains any of these words</option>
								<option value="allwords">Contains all of these words</option>
							</select>

							<input type="text" name="journal_name" id="journal_name">

						</div>

						<div class="advance_search_field">
						
							<label>Publication year</label>

							<select id="" name="include_publication_year" class="adv-select">
								<option value="less">Is earlier than</option>
								<option value="equal" selected="selected">Is equal to</option>
								<option value="greater">Is later than</option>
								<option value="between">Is between</option>
							</select>

							<input type="text" name="publication_year" id="pulication_year">

						</div>

						<div class="advance_search_field">
						
							<label for=""> Paper title </label>

				  			<select name="include_paper_name"  class="adv-select">
				  				<option value="contains">Contains the phrase</option>
				  				<option value="word">Contains any of these words</option>
				  				<option value="allwords" selected="selected">Contains all of these words</option>
				  			</select>

				  			<input type="text" name="s" id="s">

				  		</div>



				  		<div class="advance_search_field">
				  			<input type="hidden" name="post_type" value="publication" />
			  				<input class="" type="submit" id="" name="" value="Search">
			  			</div>

					</form>