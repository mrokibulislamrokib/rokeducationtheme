<?php 	/* Template Name: Teacher Template */get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="teacher-page-content">	
			
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

			<?php
				  $paged=(get_query_var('paged')) ? get_query_var('paged') : 1;
				 
				  $query_args = array(
				    'post_type' => 'teacher',
				    'posts_per_page' => 12,
				    'paged' => $paged,
				   'tax_query' => array(
					array(
					  'taxonomy' => 'exfaculty_member',
					  'field'    => 'slug',
                                          'terms'    => array( 16),
					  'operator' => 'NOT IN',
					),
                                    ),
				  );
				  $the_query = new WP_Query( $query_args );
			?>

			<?php 	
					if($the_query->have_posts()) : 
						while($the_query->have_posts()) : $the_query->the_post(); 

							global $post;
						    $name= get_post_meta(get_the_ID(), '_cmb_name', true); 
						    $designation= get_post_meta($post->ID, '_cmb_designation', true); 
						    $Office= get_post_meta($post->ID, '_cmb_Office', true);
						    $phone= get_post_meta($post->ID, '_cmb_phone', true);
						    $email= get_post_meta($post->ID, '_cmb_email', true);
						    $website= get_post_meta($post->ID, '_cmb_website', true);
						    $education= get_post_meta($post->ID, '_cmb_education', true);
						    $research= get_post_meta($post->ID, '_cmb_research_interests', true);
			?>
			

<?php 
                      //  require( locate_template( 'template-parts/content-teacher.php'));
                        //get_template_part( 'content', 'publication' ); 
                        ?>

	<div class="teacher_by_speciality faculty">

				
		<div class="fgrid1">

			<div class="customfig">
			
				<div class="views-field views-field-picture">        
			  		<div class="field-content">
			  			<?php  if ( has_post_thumbnail() ) :
			 				 the_post_thumbnail('teacher_thumb_image',array('class'=>'align','width'=>'340','height'=>'340')); 
			 			endif; ?>
					</div>  
			  	</div> 


			</div> 

		</div>

	  	<div class="figcontent fgrid1">
	  		<h3>  <a href="<?php the_permalink(); ?>"> <?php echo $name; ?></a></h3>
		    <p> <?php echo $designation; ?> </p> 
		    <ul class="timeline">
            	<?php 
					if(!empty($education)){
						foreach ($education as $edu) {
							echo "<li>". $edu."</li>";
						} 
					}	
				?>
           	</ul>
	  	</div>
			    <!-- <p> <?php //echo $Office; ?> <?php //echo $phone; ?>, <a href=""> <?php //cho $email; ?> </a> </p>
			    								 -->
		<div class="fgrid2">
			<p> <?php echo  $research; ?> </p>
		</div>

	</div>


			<?php endwhile; ?>

			<?php  wp_reset_postdata(); ?>

			<?php if ($the_query->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>

				<div class="clear"></div>

				<?php
					
					$pagination_args = array(
					  'base'            => get_pagenum_link(1) . '%_%',
					  'format'          => 'page/%#%',
					  'total'           => $numpages,
					  'current'         => $paged,
					  'show_all'        => False,
					  'end_size'        => 1,
					  'mid_size'        => $pagerange,
					  'prev_next'       => True,
					  'prev_text'       => __('&laquo;'),
					  'next_text'       => __('&raquo;'),
					  'type'            => 'plain',
					  'add_args'        => false,
					  'add_fragment'    => ''
					);

					$paginate_links = paginate_links($pagination_args);

					if ($paginate_links) {
					  echo "<nav class='custom-pagination'>";
					    echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
					    echo $paginate_links;
					  echo "</nav>";
					}
				?>

				<div class="pagenavigation">

				    <?php
						if (function_exists("custom_pagination")) {
				     		 custom_pagination($the_query->max_num_pages,"",$paged);
				      	}
				    ?>
			    </div>

			<?php } ?>

			<?php else: ?>
				<?php _e('Sorry, no posts matched your criteria.'); ?>
			<?php endif; ?>
		</div>
	</main>
</div>


	<?php do_action( 'education_hub_action_sidebar' ); ?>

<?php get_footer(); ?>