<?php 
	/* Template Name: Gallery Template */
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="gallery-page-content">		

				<?php
				  $paged=(get_query_var('paged')) ? get_query_var('paged') : 1;
				 
				  $query_args = array(
				    'post_type' => 'gallery',
				    'posts_per_page' => 12,
				    'paged' => $paged
				  );
				  $the_query = new WP_Query( $query_args );
				?>

			<?php 	
					if($the_query->have_posts()) : 
						while($the_query->have_posts()) : $the_query->the_post(); 

							global $post;	 

			?>
						<div class="gallery-content-3">

							<div class="customfig">
			    				<div class="views-field views-field-picture">        
							  		<div class="field-content">
							  			<?php  if (has_post_thumbnail()) :
							 				 the_post_thumbnail('gallery_thumb_image',array('class'=>'align','width'=>'340','height'=>'340')); 
							 			endif; ?>
									</div>  
							  	</div>  
							  	<div class="figcontent">
							  		<?php the_title( sprintf( '<h3><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?> <p> <?php echo $designation; ?> </p> 
							  	</div>
							</div>
						</div>

			<?php endwhile; ?>

			<?php  wp_reset_postdata(); ?>

			<?php if ($the_query->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>

				<div class="clear"></div>

				<?php
					
					$pagination_args = array(
					  'base'            => get_pagenum_link(1) . '%_%',
					  'format'          => 'page/%#%',
					  'total'           => $numpages,
					  'current'         => $paged,
					  'show_all'        => False,
					  'end_size'        => 1,
					  'mid_size'        => $pagerange,
					  'prev_next'       => True,
					  'prev_text'       => __('&laquo;'),
					  'next_text'       => __('&raquo;'),
					  'type'            => 'plain',
					  'add_args'        => false,
					  'add_fragment'    => ''
					);

					$paginate_links = paginate_links($pagination_args);

					if ($paginate_links) {
					  echo "<nav class='custom-pagination'>";
					    echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
					    echo $paginate_links;
					  echo "</nav>";
					}
				?>

				<div class="pagenavigation">

				    <?php
						if (function_exists("custom_pagination")) {
				     		 custom_pagination($the_query->max_num_pages,"",$paged);
				      	}
				    ?>
			    </div>

			<?php } ?>

			<?php else: ?>
				<?php _e('Sorry, no posts matched your criteria.'); ?>
			<?php endif; ?>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

	<?php  do_action( 'education_hub_action_sidebar' ); ?>

<?php get_footer(); ?>
