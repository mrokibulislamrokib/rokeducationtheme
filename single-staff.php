<?php get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php if(have_posts()):
					while ( have_posts() ) : the_post(); 
				 
					 global $post;
				     $name= get_post_meta(get_the_ID(), '_cmb_staffname', true); 
				     $designation= get_post_meta($post->ID, '_cmb_staffdesignation', true); 
				     $phone= get_post_meta($post->ID, '_cmb_staffphone', true);
			?>

				<div class="cs-teacher teacher-plain">
		            <div class="teacher-list">
		                <div class="activedetail">
		                    <div class="media">
		                       	<div class="media-left">
		                             <?php  if ( has_post_thumbnail() ) : the_post_thumbnail('staff_thumb_image',array('class'=>'align')); endif; ?>
		                        </div>
		                        <div class="media-body">
		                            <h4>   <?php echo $name; ?></h4>
		                            <h3><?php echo $designation; ?></h3>
		                            <div class="contactdiv_detail">
		                                <ul>
		                                    <li>
		                                            <ul>
		                                                <li><i class="icon-envelope4"></i></li>
		                                                <li>Email:  <?php echo $name;?> </li>
		                                            </ul>
		                                    </li>
		                                </ul>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>

		<?php 	endwhile; // End of the loop.
			  endif;
		?> 

		</main><!-- #main -->
	</div><!-- #primary -->

<?php do_action( 'education_hub_action_sidebar' ); ?>

<?php get_footer(); ?>
