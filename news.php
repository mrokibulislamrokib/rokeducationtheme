<?php 	/* Template Name: Teacher Template */get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="teacher-page-content">	
			
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>


			<?php
				  $paged=(get_query_var('paged')) ? get_query_var('paged') : 1;
				 
				  $query_args = array(
				    'post_type' => 'news',
				    'posts_per_page' =>6,
				    'paged' => $paged
				  );

				  $the_query = new WP_Query( $query_args );
			?>

			<?php 
				if($the_query->have_posts()) : 
					while($the_query->have_posts()) : $the_query->the_post(); 

						global $post;
			?>

				
				<div class="news-post">
			  		<a href="<?php the_permalink();?>"> 
			  			<?php  if ( has_post_thumbnail() ) :
			  				the_post_thumbnail('teacher_thumb_image',array('class'=>'align')); 
				 			endif; ?> 
				 	</a>
			  		<div class="news-content">
			  			<?php the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>
					  	<p> <?php // the_excerpt(20); ?> </p>

<?php

    $content = get_the_content();
    $trimmed_content = wp_trim_words( $content, 20, NULL );
    echo $trimmed_content;

?>
  					</div> <!-- .news-content -->
			  	</div>


			<?php endwhile;


		//	kriesi_pagination(); ?>

			<?php  wp_reset_postdata(); 

				//kriesi_pagination();
			?>

			<?php if ($the_query->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>

				<div class="clear"></div>

				<?php



					$pagination_args = array(
					  'base'            => get_pagenum_link(1) . '%_%',
					  'format'          => 'page/%#%',
					  'total'           => $numpages,
					  'current'         => $paged,
					  'show_all'        => False,
					  'end_size'        => 1,
					  'mid_size'        => $pagerange,
					  'prev_next'       => True,
					  'prev_text'       => __('&laquo;'),
					  'next_text'       => __('&raquo;'),
					  'type'            => 'plain',
					  'add_args'        => false,
					  'add_fragment'    => ''
					);

					$paginate_links = paginate_links($pagination_args);

					if ($paginate_links) {
					  echo "<nav class='custom-pagination'>";
					    echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
					    echo $paginate_links;
					  echo "</nav>";
					}





				?>

				<div class="pagenavigation">

				    <?php


				      if (function_exists("custom_pagination")) {
				     		 custom_pagination($the_query->max_num_pages,"",$paged);
				      }
				    ?>

  					
			      <?php //echo get_next_posts_link( 'Older Entries', $the_query->max_num_pages ); // display older posts link ?>
			      <?php //echo get_previous_posts_link( 'Newer Entries' ); // display newer posts link ?>
			    </div>

			<?php } ?>

			<?php // wp_reset_query(); ?>

			<?php else: ?>
				<?php _e('Sorry, no posts matched your criteria.'); ?>
			<?php endif; ?>
		</div>
	</main>
</div>


	<?php do_action( 'education_hub_action_sidebar' ); ?>

<?php get_footer(); ?>


