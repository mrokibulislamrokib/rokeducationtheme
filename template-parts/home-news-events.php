<?php
	$news = education_hub_get_home_news_block_content();
	$events = education_hub_get_home_events_block_content();
?>
<?php if ( $news || $events ) : ?>
	<div id="featured-news-events">
		<div class="container">
			<div class="inner-wrapper">
	    <div class="meassage_widget">
		
		<h2> Dr A.K.M Royhan Uddin </h2>
		
		<h2> Head of the Department </h2>

	    	<p align="justify">Welcome to the Department of Chemistry. It is one of the promising and emerging departments of Comilla University which was established in 2010. It is becoming familiar gradually as a noted place to academia and researchers. There are nine highly qualified enlightened teachers who are engaged in different teaching, project and research programs. Academic up-to-date curriculum of the department is designed with four branches namely physical, inorganic, organic and analytical & environmental chemistry. The laboratories both undergraduate and graduate, of the department are going to be reached with state of art technologies under the Higher Education Quality Enhancement Project (HEQEP). The laboratories are already equipped with UV-Visible spectrophotometers, Flame photometer, Kjeldahl apparatus, Vacuum evaporator, Incubator, Ovens etc.</p>
	    </div> <!-- /.widget-item -->
				<?php echo $news; ?>
				<?php echo $events; ?>
			</div> <!-- .inner-wrapper -->
		</div> <!-- .container -->
	</div> <!-- #featured-news-events -->
<?php endif ?>