<div class="publication content">
    <div class="publication-title">
    <h4> <a href="<?php the_permalink();?>"> <?php echo $name; ?> </a> </h4>

    <div class="pub_content">
        <p> <?php echo $journalname; ?> </p> 
        <p> <?php echo $pubyear; ?> </p>
         <ul class="publication">
            <?php
                if(!empty($tauthor)){
                    foreach ($tauthor as $key => $value) {
                        $teachseries = new WP_Query( array(
                            'post_type' => 'teacher',
                            'p' =>$value,
                            'nopaging' => true
                        ));
                        if ( $teachseries-> have_posts() ) { 
                            while ( $teachseries->have_posts() ) { $teachseries->the_post(); 
                                $teacher_name= get_post_meta(get_the_ID(), '_cmb_name', true); 

            ?>
                                            
                <li><a href="<?php the_permalink(); ?>"> <?php echo $teacher_name; ?> </a></li>
                                        

            <?php   } 
                            
                    }

                }

            }
            ?>

        </ul>
     </div>
        
        <p class="DOI"> <?php echo $DOI; ?> </p>

    </div>
</div>