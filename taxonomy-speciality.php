<?php 	/* Template Name: speciality Template */get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="teacher-page-content">	
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

					
	<?php
		$cat_args = array('orderby'=>'term_id','order'=> 'ASC','hide_empty'    => true, );
		
		$terms = get_terms('speciality', $cat_args);

		foreach($terms as $taxonomy){
         	
         	$term_id = $taxonomy->term_id;

		    $tax_post_args = array(
		          'post_type' => 'teacher',
		          'posts_per_page' => 999,
		          'order' => 'ASC',
		          'tax_query' => array(
		                array(
		                     'taxonomy' => 'speciality',
		                     'field' => 'term_id',
		                     'terms' => $term_id
		                )
		           )
		    );

    		$tax_post_qry = new WP_Query($tax_post_args); ?>

    		<div class="teacher_by_speciality">

    			<h4 class="tcategory"> <?php echo $taxonomy->name; ?></h4>
    		
			
			<?php 
		    if($tax_post_qry->have_posts()) :
		         while($tax_post_qry->have_posts()) :
		                $tax_post_qry->the_post();

	        			 global $post;
					     $name= get_post_meta(get_the_ID(), '_cmb_name', true); 
					     $designation= get_post_meta($post->ID, '_cmb_designation', true); 
					     $Office= get_post_meta($post->ID, '_cmb_Office', true);
					     $phone= get_post_meta($post->ID, '_cmb_phone', true);
					     $email= get_post_meta($post->ID, '_cmb_email', true);
					     $website= get_post_meta($post->ID, '_cmb_website', true); ?>
								    	
<h5>  <a href="<?php the_permalink(); ?>"> <?php echo $name; ?></a></h5>

				<?php 
                       // require( locate_template( 'template-parts/content-teacher.php'));
                        //get_template_part( 'content', 'publication' ); ?> 
		    

				<?php
		          
		            endwhile;

		            wp_reset_postdata();

				    else :
				          echo "No member";
				    endif;

		    echo "</div>";
} //end foreach loo
	?>	
			</div>
		</main>
	</div>

	<?php do_action( 'education_hub_action_sidebar' ); ?>

<?php get_footer(); ?>