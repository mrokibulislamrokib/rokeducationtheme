<?php get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="research-page-content">	
				
				<?php 
						while(have_posts()) : the_post();

							$research__teacher=get_post_meta($post->ID, 'research__teacher_meta',true);
							$research__pub=get_post_meta($post->ID, 'research__pub__meta',true);

				 ?>
					
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<header class="entry-header">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</header><!-- .entry-header -->
						<div class="entry-content">
							<?php  if ( has_post_thumbnail() ) : the_post_thumbnail('',array('class'=>'align')); endif; ?>
							<?php the_content(); ?>
						</div><!-- .entry-content -->
					</article><!-- #post-## -->

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php do_action( 'education_hub_action_sidebar' ); ?>

<?php get_footer(); ?>
