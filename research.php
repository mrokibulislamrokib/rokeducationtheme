<?php 
	/* Template Name: Research Template */
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="research-page-content">			
			<?php 
				$args = array('post_type' => 'research','posts_per_page' => 12);
				$research= new WP_Query($args);
				if($research->have_posts()):
					while($research->have_posts() ) : $research->the_post(); ?>
						<div class="research-content-">

						</div>

				<?php	endwhile; 

						wp_reset_postdata();

						else :
				          echo "No post";
				    	endif;
				?>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

	<?php  do_action( 'education_hub_action_sidebar' ); ?>

<?php get_footer(); ?>
