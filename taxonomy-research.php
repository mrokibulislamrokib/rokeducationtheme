<?php 	/* Template Name: Researh group Template */ get_header(); ?>
	
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="teacher-page-content">	
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

	<?php
		$cat_args = array('orderby'=>'term_id','order'=> 'ASC','hide_empty'    => true, );
		$terms = get_terms('research', $cat_args);
		foreach($terms as $term){?>
			<a href="<?php echo $term->link;?>"> <?php echo $term->name; ?> </a> 
			<?php
				$term_id = $term->term_id;
				$tax_post_args = array(
			          'post_type' => 'teacher',
			          'posts_per_page' => 999,
			          'order' => 'ASC',
			          'tax_query' => array(
			                array(
			                     'taxonomy' => 'research',
			                     'field' => 'term_id',
			                     'terms' => $term_id
			                )
			           )
			    );
    			$tax_post_qry = new WP_Query($tax_post_args);

		    	if($tax_post_qry->have_posts()) :
		         	while($tax_post_qry->have_posts()) :
		                $tax_post_qry->the_post();
		            		global $post;
						     $name= get_post_meta(get_the_ID(), '_cmb_name', true); 
						     $designation= get_post_meta($post->ID, '_cmb_designation', true); 
						     $Office= get_post_meta($post->ID, '_cmb_Office', true);
						     $phone= get_post_meta($post->ID, '_cmb_phone', true);
						     $email= get_post_meta($post->ID, '_cmb_email', true);
						     $website= get_post_meta($post->ID, '_cmb_website', true);
							 if ( has_post_thumbnail() ) :
			 				 	the_post_thumbnail('teacher_thumb_image',array('class'=>'align','width'=>'340','height'=>'340')); 
			 				 endif; ?>
		        <h3>  <a href="<?php the_permalink(); ?>"> <?php echo $name; ?>  </a>   </h3>
						    	

				

				<?php
		          endwhile;

		    else :
		          echo "No member";
		    endif;	
    	} ?>

			</div>
		</div>
	</main>
</div>

	<?php do_action( 'education_hub_action_sidebar' ); ?>

<?php get_footer(); ?>
