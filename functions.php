<?php
/**
 * Education Hub functions and definitions.
 *
 * @link https://codex.wordpress.org/Functions_File_Explained
 *
 * @package Education_Hub
 */

if ( ! function_exists( 'education_hub_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function education_hub_setup() {
		/*
		 * Make theme available for translation.
		 */
		load_theme_textdomain( 'education-hub' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'education-hub-thumb', 360, 270 );
        add_image_size('teacher_thumb_image',340,340);
        add_image_size('teacher_small_image',220,220);
        add_image_size('teacher_large_image',440,440);
        add_image_size('staff_thumb_image',340,340);
        add_image_size('staff_small_image',340,340);
        add_image_size('staff_large_image',440,440);
        add_image_size('gallery_thumb_image',340,340);
        add_image_size('gallery_small_image',340,340);
        add_image_size('gallery_large_image',440,440);
        add_image_size('event_thumb_image',340,340);
        add_image_size('event_small_image',340,340);
        add_image_size('event_large_image',440,440);

		// This theme uses wp_nav_menu() in four location.
		register_nav_menus( array(
			'primary'     => esc_html__( 'Primary Menu', 'education-hub' ),
			'footer'      => esc_html__( 'Footer Menu', 'education-hub' ),
			'social'      => esc_html__( 'Social Menu', 'education-hub' ),
			'quick-links' => esc_html__( 'Quick Links Menu', 'education-hub' ),
			'notfound'    => esc_html__( '404 Menu', 'education-hub' ),
                        'our_people'  => esc_html__( 'Our People', 'education-hub' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		/*
		 * Enable support for Post Formats.
		 * See https://developer.wordpress.org/themes/functionality/post-formats/
		 */
		add_theme_support( 'post-formats', array(
			'aside',
			'image',
			'video',
			'quote',
			'link',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'education_hub_custom_background_args', array(
			'default-color' => 'dfdfd0',
			'default-image' => '',
		) ) );

		/*
		 * Enable support for custom logo.
		 */
		add_theme_support( 'custom-logo', array(
			'flex-height' => true,
			'flex-width'  => true,
		) );

		/*
		 * Enable support for selective refresh of widgets in Customizer.
		 */
		add_theme_support( 'customize-selective-refresh-widgets' );

		$min = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

		// Editor style.
		add_editor_style( 'css/editor-style' . $min . '.css' );

		// Enable support for footer widgets.
		add_theme_support( 'footer-widgets', 4 );

		// Load Supports.
		require get_template_directory() . '/inc/support.php';

		global $education_hub_default_options;
		$education_hub_default_options = education_hub_get_default_theme_options();

	}
endif;

add_action( 'after_setup_theme', 'education_hub_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function education_hub_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'education_hub_content_width', 640 );
}
add_action( 'after_setup_theme', 'education_hub_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function education_hub_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Primary Sidebar', 'education-hub' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here to appear in your Primary Sidebar.', 'education-hub' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Secondary Sidebar', 'education-hub' ),
		'id'            => 'sidebar-2',
		'description'   => esc_html__( 'Add widgets here to appear in your Secondary Sidebar.', 'education-hub' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Teacher Sidebar', 'education-hub' ),
		'id'            => 'sidebar-3',
		'description'   => esc_html__( 'Add widgets here to appear in your Secondary Sidebar.', 'education-hub' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
/*
        register_sidebar( array(
		'name'          => esc_html__( 'News && Event Sidebar', 'education-hub' ),
		'id'            => 'sidebar-3',
		'description'   => esc_html__( 'Add widgets here to appear in your Secondary Sidebar.', 'education-hub' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
        
        register_sidebar( array(
		'name'          => esc_html__( 'Teacher Sidebar', 'education-hub' ),
		'id'            => 'sidebar-4',
		'description'   => esc_html__( 'Add widgets here to appear in your Secondary Sidebar.', 'education-hub' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );*/


}
add_action( 'widgets_init', 'education_hub_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function education_hub_scripts() {
	$min = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

	wp_enqueue_style( 'fontawesome', get_template_directory_uri() . '/third-party/font-awesome/css/font-awesome' . $min . '.css', '', '4.6.1' );
	wp_enqueue_style( 'education-hub-google-fonts', '//fonts.googleapis.com/css?family=Open+Sans:600,400,400italic,300,100,700|Merriweather+Sans:400,700' );

	wp_enqueue_style( 'education-hub-style', get_stylesheet_uri(), array(), '1.8' );

	if ( has_header_image() ) {
		$custom_css = '#masthead{ background-image: url("' . esc_url( get_header_image() ) . '"); background-repeat: no-repeat; background-position: center center; }';
		wp_add_inline_style( 'education-hub-style', $custom_css );
	}

	wp_enqueue_script( 'education-hub-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix' . $min . '.js', array(), '20130115', true );

	wp_enqueue_script( 'cycle2', get_template_directory_uri() . '/third-party/cycle2/js/jquery.cycle2' . $min . '.js', array( 'jquery' ), '2.1.6', true );

	wp_enqueue_script( 'education-hub-custom', get_template_directory_uri() . '/js/custom' . $min . '.js', array( 'jquery' ), '1.0', true );

	wp_register_script( 'education-hub-navigation', get_template_directory_uri() . '/js/navigation' . $min . '.js', array(), '20120206', true );
	wp_localize_script( 'education-hub-navigation', 'EducationHubScreenReaderText', array(
		'expand'   => '<span class="screen-reader-text">' . __( 'expand child menu', 'education-hub' ) . '</span>',
		'collapse' => '<span class="screen-reader-text">' . __( 'collapse child menu', 'education-hub' ) . '</span>',
	) );
	wp_enqueue_script( 'education-hub-navigation' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
/*
	if (is_page() && basename(get_page_template())=="teacher.php") {
		wp_enqueue_style( 'bootstrap_css', get_template_directory_uri() .'/css/bootstrap.css');
		wp_enqueue_script('bootstrap_js', get_template_directory_uri() .'/js/bootstrap.min.js');
	}*/
}
add_action( 'wp_enqueue_scripts', 'education_hub_scripts' );

/**
 * Load init.
 */
require get_template_directory() . '/inc/init.php';









function custom_pagination($numpages = '', $pagerange = '', $paged='') {

  if (empty($pagerange)) {
    $pagerange = 2;
  }

  global $paged;
  
  if (empty($paged)) {
    $paged = 1;
  }
  
  if($numpages == '') {
    global $wp_query;
    $numpages = $wp_query->max_num_pages;
    if(!$numpages) {
        $numpages = 1;
    }
  }

  /** 
   * We construct the pagination arguments to enter into our paginate_links
   * function. 
   */
  $pagination_args = array(
    'base'            => get_pagenum_link(1) . '%_%',
    'format'          => 'page/%#%',
    'total'           => $numpages,
    'current'         => $paged,
    'show_all'        => False,
    'end_size'        => 1,
    'mid_size'        => $pagerange,
    'prev_next'       => True,
    'prev_text'       => __('&laquo;'),
    'next_text'       => __('&raquo;'),
    'type'            => 'plain',
    'add_args'        => false,
    'add_fragment'    => ''
  );

  $paginate_links = paginate_links($pagination_args);

  if ($paginate_links) {
    echo "<nav class='custom-pagination'>";
      echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
      echo $paginate_links;
    echo "</nav>";
  }

}




/*function template_chooser($template){
  global $wp_query;
  $post_type = get_query_var('post_type');
  if( $wp_query->is_search && $post_type =='publication'){
    return locate_template('publication-search.php');
  }
  return $template;
}
add_filter('template_include', 'template_chooser');*/


/*function publicationsearchfilter($query) {

//	if ( ! $q->is_main_query() ) return;
	
//	if ( ! $q->is_post_type_archive() ) return;
		
	//if ( ! is_admin()) {
    
    if ($query->is_search && !is_admin() ) {
       

        if(isset($_GET['post_type'])) {
            $type = $_GET['post_type'];
                if($type == 'publication') {
                    $query->set('post_type',array('publication'));
                }
        } 



        $s = $_GET['s'];
        
        $meta_query=array();

       	$author=isset($_GET['']) ? $_GET[''] : '';
       	$journal= isset($_GET['']) ? $_GET[''] : '';
       	$paper= isset($_GET['paper']) ? $_GET['paper'] : '';
       	$publication_year= isset($_GET['']) ? $_GET[''] : '';

        if (!empty($paper)) { 
           		 
       		array_push($meta_query,array(
                'key' => '_cmb_papername',
                'value' => '$paper',
                'compare' => 'LIKE',
            ));
        }


        if (!empty($journal)) { 
           		 
       		array_push($meta_query,array(
                'key' => '',
                'value' => '',
            ));
        }

        if (!empty($publication_year)) { 
           		 
       		array_push($meta_query,array(
                'key' => '',
                'value' => '',
            ));
        }


        if (!empty($author)) { 
           		 
       		array_push($meta_query,array(
                'key' => '',
                'value' => '',
            ));
        }

    }
	
	return $query;
}


add_filter('pre_get_posts','publicationsearchfilter');



function searchs_filter($query) {
    
    if ( !is_admin() && $query->is_main_query() ) {
        
        if ($query->is_search) {

            /*$meta_args = array(
                'relation' => 'OR',
                array(
                    'key' => 'treatments',
                    'value' => $s,
                    'compare' => 'LIKE',
                ),
                array(
                    'key' => 'address',
                    'value' => $s,
                    'compare' => 'LIKE',
                ),
            );

            
            
            //$query->set('meta_query', $meta_args);
        }
    }
}

add_action('pre_get_posts','searchs_filter');*/




/*function search_filter($query) {
  	
  	if( !is_admin() && $query->is_main_query() ) {
    	
    	if($query->is_search) {
      		
      		//$query->set('post_type', array( 'post', 'news','event') );
      		if(isset($_GET['post_type'])) {
            	$type = $_GET['post_type'];
                
                if($type == 'publication') {
                   

                   $query->set('post_type',array('publication'));
                
                   

                   $papername=isset($_GET['s']) ? $_GET['s'] : '';

                   	if(!empty($papername)){
                   	
	                   	$meta_query_args = array(
					      array(
					        'key' => '_cmb_papername',
					        'value' => $papername,
					        'compare' => 'LIKE',
					      ),
					    );


    					$query->set('meta_query', $meta_query_args);

    				}
                
                }
       		} 
      		
      		//$query->set('post_type', 'publication');
    	}
  	}

  	return $query;
}

add_action('pre_get_posts','search_filter');*/


function searchfilter($query) {


	
	// 
		
/*		if(isset($_GET['post_type'])) {
            
            $type = $_GET['post_type'];

            if($type == 'publication') {*/
                   

              //  $query->set('post_type',array('publication'));

          //  }

        //}


     /*    $query->set('post_type', 'publication');

	} else { 

		$query->set('post_type', 'post'); 
	}*/

	if($query->is_search()) {
		
		
		$s = $_GET['s'];
		
		$publication_year=isset($_GET['publication_year']) ? $_GET['publication_year'] : '';

		echo $publication_year;

		$journal_name=isset($_GET['journal_name']) ? $_GET['journal_name']  : '';

		$author_name=isset($_GET['author_name']) ?  $_GET['author_name']  : '';


		$include_author_type  = isset($_GET['include_author_name']) ?  $_GET['include_author_name'] :'';
		$include_journal_type = isset($_GET['include_journal_name'])?  $_GET['include_journal_name'] :'';
		$include_publication_year_type= isset($_GET['include_publication_year']) ?  $_GET['include_publication_year'] :'';
		$include_paper_type = isset($_GET['include_paper_name']) ? $_GET['include_paper_name']:'';


//		$meta_query=array();
               
        //$query->set('s', '');

        $query->set('post_type', 'publication');

        if(!empty($s)){

        /*	if($include_publication_year_type=='word'){

        		 array_push($meta_query, array(
	                'key' => '_cmb_papername',
	                'value' => $s,
	                'compare' => 'LIKE',
	            ));

        	} else if($include_publication_year_type=='allwords'){

        		 array_push($meta_query, array(
	                'key' => '_cmb_papername',
	                'value' => $s,
	                'compare' => '=',
	            ));

        	}*/


           
        }


        if(!empty($publication_year)){

        	/*if($include_publication_year_type=='less'){

        		array_push($meta_query, array(
	                'key' => '_cmb_pubyear',
	                'value' => $publication_year,
	                'compare' => '<',
	                'type'    =>'DATETIME'
	            ));

        	} else if($include_publication_year_type=='equal'){
				
				array_push($meta_query, array(
	                'key' => '_cmb_pubyear',
	                'value' => $publication_year,
	                'compare' => 'LIKE',
	                'type'    =>'DATE'
	            ));

        	} else if($include_publication_year_type=='greater'){
				
				array_push($meta_query, array(
	                'key' => '_cmb_pubyear',
	                'value' => $publication_year,
	                'compare' => '>',
	                'type'    =>'DATE'
	            ));


        	} else if($include_publication_year_type=='between'){

				array_push($meta_query, array(
	                'key' => '_cmb_pubyear',
	                'value' => $publication_year,
	                'compare' => 'BETWEEN',
	                'type'    =>'DATETIME'
            	));

        	}*/


        /*	array_push($meta_query, array(
	                'key' => '_cmb_pubyear',
	                'value' => $publication_year,
	                'compare' => 'LIKE',
	               'type'    =>'DATE'
	            ));*/
        	
        }


        if(!empty($journal_name)){

        	/*if($include_journal_type=='words'){

        		array_push($meta_query, array(
	                'key' => '_cmb_journalname',
	                'value' => $journal_name,
	                'compare' => 'LIKE',
	            ));
        	}

        	} else if($include_journal_type=='allwords'){

				array_push($meta_query, array(
	                'key' => '_cmb_journalname',
	                'value' => $journal_name,
	                'compare' => '=',
	            ));

        	}*/

        /*	array_push($meta_query, array(
	                'key' => '_cmb_journalname',
	                'value' => $journal_name,
	                'compare' => 'LIKE',
	            ));*/

        }


        if(!empty($author_name)){

        	/*if($include_journal_type=='words'){

        		array_push($meta_query, array(
	                'key' => '_cmb_author',
	                'value' => $author_name,
	                'compare' => 'LIKE',
	            ));
        	}

        	} else if($include_journal_type=='allwords'){

				array_push($meta_query, array(
	                'key' => '_cmb_author',
	                'value' => $author_name,
	                'compare' => 'IN',
	            ));

        	}*/

        	/*array_push($meta_query, array(
	                'key' => '_cmb_author',
	                'value' => $author_name,
	                'compare' => 'IN',
	            ));*/
        }

        $query->set('meta_query',$meta_query);

	}else{

		//$query->set('post_type','post'); 
	}

	return $query;
}

//add_filter('pre_get_posts','searchfilter');


/*
function wpse_load_custom_search_template(){
    if( isset($_REQUEST['search']) == 'publication' ) {
        require('advance-search-result.php');
        die();
    }
}
add_action('init','wpse_load_custom_search_template');
*/


function wpse_load_custom_search_template(){ 
	
	if(isset($_REQUEST['search']) == 'advanced' ) { 
		
		require('advance-search-result.php'); 
		
		die(); 
	} 
} 

add_action('init','wpse_load_custom_search_template'); 





add_filter( 'get_meta_sql', 'pub_get_meta_sql' );


function pub_get_meta_sql($meta_sql){
   
   //$meta_sql['join'] = " LEFT JOIN wp_postmeta ON (wp_posts.ID = wp_postmeta.post_id AND wp_postmeta.meta_key = 'pub_teachers_meta') ";
   //$meta_sql['where'] = " AND (wp_postmeta.meta_value IS NULL OR wp_postmeta.meta_value < '" . date('Y-m-d') . "')";
    return $meta_sql;
}


/*function custom_search_where($where) {
    // put the custom fields into an array
    $customs = array('custom_field1', 'custom_field2', 'custom_field3');

    foreach($customs as $custom) {
	$query .= " OR (";
	$query .= "(m.meta_key = '$custom')";
	$query .= " AND (m.meta_value  LIKE '{$n}{$term}{$n}')";
        $query .= ")";
    }

    $where = " AND ({$query}) AND ($wpdb->posts.post_status = 'publish') ";
    return($where);
}
add_filter('posts_where', 'custom_search_where');*/
?>