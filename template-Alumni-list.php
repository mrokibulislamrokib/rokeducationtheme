<?php 
	
	/* Template Name: Alumni Template */
	
	get_header(); 

?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
           
               <a href="<?php echo esc_url( add_query_arg( 'c', $my_value_for_c, site_url( '/add_alumni_book/' ) ) )?>"> Add  New Alumni </a>

               <br/>
                
                <br/>
        
               		<?php
				  $paged=(get_query_var('paged')) ? get_query_var('paged') : 1;	 
				  $query_args = array(
				    'post_type' => 'Alumni',
				    'posts_per_page' => 12,
				    'paged' => $paged
				  );
				  $the_query = new WP_Query( $query_args );
			?>

			<?php 	
				if($the_query->have_posts()) : 
					
					while($the_query->have_posts()) : $the_query->the_post(); 
					
					global $post;
					
					$name=get_post_meta( $post->ID,'_cmb_alumniname',true);
					$phone=get_post_meta( $post->ID,'_cmb_alumniphone',true);
					$email=get_post_meta( $post->ID,'_cmb_alumniemail',true);
                                        $session=get_post_meta( $post->ID,'_cmb_alumnisession',true);
					$job_position=get_post_meta( $post->ID,'_cmb_alumnijobposition',true);
			 ?>
				
				<table>
    <thead>
        <tr>
        	<th>Name  </th>
        	<th>Email</th>
        	<th>Phone</th>
        	<th>Session</th>
        	<th>Current Job Position</th>
    	</tr>
    </thead>
    <tbody>
        <tr>
            <td> <?php echo $name;?>  </td>
            <td> <?php echo $phone;?> </td>
            <td> <?php echo $email;?>  </td>
            <td> <?php echo $session;?> </td>
            <td> <?php echo $job_position;?> </td>
        </tr>
    </tbody>
</table>
			
			<?php endwhile; ?>

			<?php  wp_reset_postdata(); ?>

			<?php if ($the_query->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>

				<div class="clear"></div>

				<?php
					
					$pagination_args = array(
					  'base'            => get_pagenum_link(1) . '%_%',
					  'format'          => 'page/%#%',
					  'total'           => $numpages,
					  'current'         => $paged,
					  'show_all'        => False,
					  'end_size'        => 1,
					  'mid_size'        => $pagerange,
					  'prev_next'       => True,
					  'prev_text'       => __('&laquo;'),
					  'next_text'       => __('&raquo;'),
					  'type'            => 'plain',
					  'add_args'        => false,
					  'add_fragment'    => ''
					);

					$paginate_links = paginate_links($pagination_args);

					if ($paginate_links) {
					  echo "<nav class='custom-pagination'>";
					    echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
					    echo $paginate_links;
					  echo "</nav>";
					}
				?>

				<div class="pagenavigation">

				    <?php
						if (function_exists("custom_pagination")) {
				     		 custom_pagination($the_query->max_num_pages,"",$paged);
				      	}
				    ?>
			    </div>

			<?php } ?>

			<?php else: ?>
				<?php _e('Sorry, no posts matched your criteria.'); ?>
			<?php endif; ?>


	</main>
</div>

<?php get_footer(); ?>