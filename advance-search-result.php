<?php get_header(); ?>
<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <div class="pub-page-content">  
            <div class="advance_search_form">   
                <?php get_template_part('advanced','searchform' ); ?>
            </div>

<?php
        global $wpdb;
        $_name = $_GET['name'] != '' ? $_GET['name'] : '';
        $_model = $_GET['model'] != '' ? $_GET['model'] : '';        
        $publication_year = $_GET['publication_year'] != '' ? $_GET['publication_year'] : '';
        $journal_name = $_GET['journal_name'] != '' ? $_GET['journal_name'] : '';
        $publication_teacher_array=array();
        $post_pub=array();
        $pub_teacher=array();

        if(!empty($_model)){
        
            $sql="SELECT `post_id` FROM `wp_postmeta` WHERE meta_key='pub_teachers_meta'";
            $pub_teacher_sql=$wpdb->get_results($sql);
            $record_num= count($pub_teacher_sql);
           
            for($i=0;$i<$record_num;$i++){
                $publication_teacher_array[$pub_teacher_sql[$i]->post_id]=get_post_meta($pub_teacher_sql[$i]->post_id, 'pub_teachers_meta',true);
                if (count(array_diff($_model,$publication_teacher_array[$pub_teacher_sql[$i]->post_id]))== 0) {
                    $post_pub[$i]=$pub_teacher_sql[$i]->post_id;
                }
            }
            print_r($post_pub);
            echo ' search from teacher <br/>';
        }
?> 


<?php
    
if(!empty($publication_year)|| !empty($journal_name) || !empty($_name)){

    $publication_args = array(
        'post_type' =>  'publication',
        'posts_per_page'=>6,
        's'         =>  $_name,
        'meta_query'    =>  array(
            'relation' =>'OR',
                array(
                    'key'=>'_cmb_pubyear',
                    'value'=>$publication_year,
                    'compare' =>'='
                ),
                array(
                    'key'=>'_cmb_journalname',
                    'value'=>$journal_name,
                    'compare'=>'LIKE'
                ),
        ),
    );

    $publicationSearchQuery = new WP_Query( $publication_args );
    
    if($publicationSearchQuery->have_posts() ) :
        while( $publicationSearchQuery->have_posts() ) : $publicationSearchQuery->the_post();   
            global $post;
            array_push($pub_teacher,$post->ID);
        endwhile;
        
        else :
          //  _e( 'Sorry, nothing matched your search criteria', 'textdomain' );
    endif;
    
    wp_reset_postdata();

    print_r($pub_teacher);

    echo ' search from others <br/>';
        
} 
  
?>


<?php  
    if(!empty($pub_teacher) && !empty($post_pub)){ 
        $atlast=array_intersect($post_pub,$pub_teacher);
    }else if(!empty($post_pub)){
        $atlast=$post_pub;
    }else if(!empty($pub_teacher)){
        $atlast=$pub_teacher;
    }

    print_r($atlast); 
?>

<?php
    if(!empty($atlast)){
        foreach ($atlast as $key => $value) {
        
            $series = new WP_Query( array(
                'post_type' => 'publication',
                'p'=>$value
            ));

        if($series-> have_posts()){ 
            while($series->have_posts()){ 
                $series->the_post();
            global $post;
            echo $post->ID;
            $name= get_post_meta(get_the_ID(), '_cmb_papername', true); 
            $journalname= get_post_meta($post->ID, '_cmb_journalname', true); 
            $pubyear= get_post_meta($post->ID, '_cmb_pubyear', true); 
            $tauthor= get_post_meta($post->ID, 'pub_teachers_meta', true); 
            $DOI= get_post_meta($post->ID, '_cmb_DOI_NUMBER', true);
            //echo the_title( $before, $after, $echo ); 
            print_r($tauthor);
        ?>
            
		<?php require( locate_template( 'template-parts/content-publication.php')); ?>
<?php   } 
                                        
    }
   
        }
    }
?>
<?php 

/********* No search option *************/

if(empty($pub_teacher) && empty($post_pub)){ 

    echo 'no search query';
    
      $paged=(get_query_var('paged')) ? get_query_var('paged') : 1;  
      $query_args = array(
        'post_type' => 'publication',
        'posts_per_page' => 2,
        'paged' => $paged
      );
      $the_query = new WP_Query( $query_args );
  
    if($the_query->have_posts()) : 
        while($the_query->have_posts()) : $the_query->the_post(); 
            global $post;
            $name= get_post_meta(get_the_ID(), '_cmb_papername', true); 
            $journalname= get_post_meta($post->ID, '_cmb_journalname', true); 
            $pubyear= get_post_meta($post->ID, '_cmb_pubyear', true);
            $tauthor= get_post_meta($post->ID, 'pub_teachers_meta', true);

?>
            <?php 
                        require( locate_template( 'template-parts/content-publication.php'));
                        //get_template_part( 'content', 'publication' ); ?>


                
            
            <?php endwhile;  wp_reset_postdata(); ?>
                
                            <?php if ($the_query->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>

                <div class="clear"></div>

                <?php
                    
                    $pagination_args = array(
                      'base'            => get_pagenum_link(1) . '%_%',
                      'format'          => 'page/%#%',
                      'total'           => $numpages,
                      'current'         => $paged,
                      'show_all'        => False,
                      'end_size'        => 1,
                      'mid_size'        => $pagerange,
                      'prev_next'       => True,
                      'prev_text'       => __('&laquo;'),
                      'next_text'       => __('&raquo;'),
                      'type'            => 'plain',
                      'add_args'        => false,
                      'add_fragment'    => ''
                    );

                    $paginate_links = paginate_links($pagination_args);

                    if ($paginate_links) {
                      echo "<nav class='custom-pagination'>";
                        echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
                        echo $paginate_links;
                      echo "</nav>";
                    }
                ?>

                <div class="pagenavigation">

                    <?php
                        if (function_exists("custom_pagination")) {
                             custom_pagination($the_query->max_num_pages,"",$paged);
                        }
                    ?>
                </div>

            <?php } ?>


            <?php else: ?>
                <?php _e('Sorry, no posts matched your criteria.'); ?>
            <?php endif; ?>



<?php }
?>
  
        </div>
    </main>
</div>

    
   

<?php

    do_action( 'education_hub_action_sidebar' );
?>

<?php get_footer(); ?>