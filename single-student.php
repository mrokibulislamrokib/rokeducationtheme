<?php get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if(have_posts()):
				while ( have_posts() ) : the_post(); 
					the_title();
					the_content();

		?>

		<?php 	endwhile; // End of the loop.
			  endif;
		?>
                <?php get_sidebar(); ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php do_action( 'education_hub_action_sidebar' ); ?>

<?php get_footer(); ?>
