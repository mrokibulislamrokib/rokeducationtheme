<?php /* Template Name: Publication Template */
get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="publication-page-content">	
			<div class="advance_search_form">	
				<?php // get_template_part( 'publication', 'searchform' ); ?>
				<?php   get_template_part( 'advanced', 'searchform' ); ?>
			</div>
 			<?php
				  $paged=(get_query_var('paged')) ? get_query_var('paged') : 1;	 
				  $query_args = array(
				    'post_type' => 'publication',
				    'posts_per_page' => 12,
				    'paged' => $paged
				  );
				  $the_query = new WP_Query( $query_args );
			?>

			<?php 	
				if($the_query->have_posts()) : 
					while($the_query->have_posts()) : $the_query->the_post(); 
						global $post;
						$name= get_post_meta(get_the_ID(), '_cmb_papername', true); 
						$journalname= get_post_meta($post->ID, '_cmb_journalname', true); 
						$pubyear= get_post_meta($post->ID, '_cmb_pubyear', true);
						// $author= get_post_meta($post->ID, '_cmb_author', true);
						$tauthor= get_post_meta($post->ID, 'pub_teachers_meta', true);
						$DOI= get_post_meta($post->ID, '_cmb_DOI_NUMBER', true);
						// $pub_teach=get_post_meta($post->ID, 'pub_teachers_meta', true); ?>


				<?php 
						require( locate_template( 'template-parts/content-publication.php'));
						//get_template_part( 'content', 'publication' ); ?>
			
			<?php endwhile; ?>

			<?php  wp_reset_postdata(); ?>

			<?php if ($the_query->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>

				<div class="clear"></div>

				<?php
					
					$pagination_args = array(
					  'base'            => get_pagenum_link(1) . '%_%',
					  'format'          => 'page/%#%',
					  'total'           => $numpages,
					  'current'         => $paged,
					  'show_all'        => False,
					  'end_size'        => 1,
					  'mid_size'        => $pagerange,
					  'prev_next'       => True,
					  'prev_text'       => __('&laquo;'),
					  'next_text'       => __('&raquo;'),
					  'type'            => 'plain',
					  'add_args'        => false,
					  'add_fragment'    => ''
					);

					$paginate_links = paginate_links($pagination_args);

					if ($paginate_links) {
					  echo "<nav class='custom-pagination'>";
					    echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
					    echo $paginate_links;
					  echo "</nav>";
					}
				?>

				<div class="pagenavigation">

				    <?php
						if (function_exists("custom_pagination")) {
				     		 custom_pagination($the_query->max_num_pages,"",$paged);
				      	}
				    ?>
			    </div>

			<?php } ?>

			<?php else: ?>
				<?php _e('Sorry, no posts matched your criteria.'); ?>
			<?php endif; ?>

		</div>
	</main>
</div>
<?php do_action( 'education_hub_action_sidebar' ); ?>

<?php get_footer(); ?>