<?php




add_action( 'cmb2_init', 'cmb2_add_metabox' );

function cmb2_add_metabox() {

	$prefix = '_cmb_';


    $event = new_cmb2_box( array(
		'id'           => $prefix . 'event_metabox',
		'title'        => __( 'Event Details', 'cmb2' ),
		'object_types' => array( 'event' ),
		'context'      => 'normal',
		'priority'     => 'default',
	) );

    $event->add_field( array(
		'name' => __( 'Speaker', 'cmb2' ),
		'id' => $prefix . 'Speaker',
		'type' => 'text',
		'repeatable' => true,
	));

    
    $contact = new_cmb2_box( array(
		'id'           => $prefix . 'contact_metabox',
		'title'        => __( 'Contact Details', 'cmb2' ),
		'object_types' => array( 'teacher' ),
		'context'      => 'normal',
		'priority'     => 'default',
	) );

	$contact->add_field( array(
		'name' => __( 'name', 'cmb2' ),
		'id' => $prefix . 'name',
		'type' => 'text',
	) );

	$contact->add_field( array(
		'name' => __( 'designation', 'cmb2' ),
		'id' => $prefix . 'designation',
		'type' => 'text',
	) );

	$contact->add_field( array(
		'name' => __( 'Office', 'cmb2' ),
		'id' => $prefix . 'Office',
		'type' => 'text',
	) );

	$contact->add_field( array(
		'name' => __( 'phone', 'cmb2' ),
		'id' => $prefix . 'phone',
		'type' => 'text',
	) );

	$contact->add_field( array(
		'name' => __( 'website', 'cmb2' ),
		'id' => $prefix . 'website',
		'type' => 'text_url',
	) );

	$contact->add_field( array(
		'name' => __( 'email', 'cmb2' ),
		'id' => $prefix . 'email',
		'type' => 'text',
		'repeatable' => true,
	) );
        
    $contact->add_field( array(
		'name' => __( 'Research Interests', 'cmb2' ),
		'id' => $prefix . 'research_interests',
		'type' => 'wysiwyg',
	) );

	$contact->add_field( array(
		'name' => __( 'Biographical Details', 'cmb2' ),
		'id' => $prefix . 'bio',
		'type' => 'wysiwyg',
		//'type' => 'textarea'

	) );

   /* $contact->add_field( array(
		'name' => __( 'publication', 'cmb2' ),
		'id' => $prefix . 'publication',
		'type' => 'textarea',
		'repeatable' => true,
	) ); */

	$contact->add_field( array(
		'name' => __( 'education & certification', 'cmb2' ),
		'id' => $prefix . 'education',
		'type' => 'text',
		'repeatable' => true,
	) );

}

add_action( 'cmb2_init', 'Staff_add_metabox' );

function Staff_add_metabox(){

	$prefix = '_cmb_';

	$cmb = new_cmb2_box( array(
		'id'           => $prefix . 'staff',
		'title'        => __( 'Staff Contact Detail', 'cmb2' ),
		'object_types' => array( 'staff' ),
		'context'      => 'normal',
		'priority'     => 'default',
	) );

	$cmb->add_field( array(
		'name' => __( 'Staff Name', 'cmb2' ),
		'id' => $prefix . 'staffname',
		'type' => 'text',
	) );

	$cmb->add_field( array(
		'name' => __( 'designation', 'cmb2' ),
		'id' => $prefix . 'staffdesignation',
		'type' => 'text',
	) );

	$cmb->add_field( array(
		'name' => __( 'phone', 'cmb2' ),
		'id' => $prefix . 'staffphone',
		'type' => 'text',
	) );

}


add_action( 'cmb2_init', 'pub_add_metabox' );

function pub_add_metabox(){

	$prefix = '_cmb_';

	$pub = new_cmb2_box( array(
		'id'           => $prefix . 'publication',
		'title'        => __( 'Publication Meta', 'cmb2' ),
		'object_types' => array( 'publication' ),
		'context'      => 'normal',
		'priority'     => 'default',
	) );

	$pub->add_field( array(
		'name' => __( 'Paper title', 'cmb2' ),
		'id' => $prefix .'papername',
		'type' => 'text',
	) );

	$pub->add_field( array(
		'name' => __( 'Journal Name', 'cmb2' ),
		'id' => $prefix . 'journalname',
		'type' => 'text',
	) );

	$pub->add_field( array(
		'name' => __( 'Publication Year', 'cmb2' ),
		'id' => $prefix . 'pubyear',
		'type' => 'text_date',
		'date_format' => 'Y',
	) );


	$pub->add_field( array(
		'name' => __( 'Author', 'cmb2' ),
		'id' => $prefix . 'author',
		'type' => 'text',
		'repeatable' => true,
	) );
	
	$pub->add_field( array(
		'name' => __( 'DOI Number', 'cmb2' ),
		'id' => $prefix . 'DOI_NUMBER',
		'type' => 'text',
	) );

}


add_action( 'cmb2_init', 'research_add_metabox' );

function research_add_metabox(){


	$prefix = '_cmb_';

	$research = new_cmb2_box( array(
		'id'           => $prefix . 'research_metabox',
		'title'        => __( 'research Details', 'cmb2' ),
		'object_types' => array( 'research'),
		'context'      => 'normal',
		'priority'     => 'default',
	));



	$group_field_id=$research->add_field( array(
	    'id'          => 'research_teacher_group',
	    'type'        => 'group',
	    'options'     => array(
	        'group_title'   => __( 'Teacher {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
	        'add_button'    => __( 'Add Another Teacher', 'cmb2' ),
	        'remove_button' => __( 'Remove Teacher', 'cmb2' ),
	        'sortable'      => true, // beta
	    ),
	) );


	$research->add_group_field( $group_field_id,array(
	    'name'             => 'Teacher Name',
	    'desc'             => 'Select Teacher Name',
	    'id'               => 'research_member_name',
	    'type'             => 'select',
	    'show_option_none' => true,
	    'default'          => 'custom',
	    'options_cb' => 'show_research_options',
	));

	$research->add_group_field($group_field_id,array(
	    'name'             => 'Member Type',
	    'desc'             => 'select Member Type',
	    'id'               => 'research_member_type',
	    'type'             => 'select',
	    'show_option_none' => true,
	    'default'          => 'custom',
	    'options'          => array(
	        'Core' => __( 'Core Member', 'cmb2' ),
	        'Associate'   => __( 'Associate Member', 'cmb2' ),
	    ),
	));

}


add_action( 'cmb2_init', 'alumni_add_metabox' );

function alumni_add_metabox(){

	$prefix = '_cmb_';

	$alumni = new_cmb2_box( array(
		'id'           => $prefix .'Alumni',
		'title'        => __( 'Alumni Meta', 'cmb2' ),
		'object_types' => array( 'Alumni' ),
		'context'      => 'normal',
		'priority'     => 'default',
	) );

	$alumni->add_field( array(
		'name' => __( 'Name', 'cmb2' ),
		'id' => $prefix . 'alumniname',
		'type' => 'text',
	));

	$alumni->add_field( array(
		'name' => __( 'Phone', 'cmb2' ),
		'id' => $prefix . 'alumniphone',
		'type' => 'text',
	));

	$alumni->add_field( array(
		'name' => __( 'Email', 'cmb2' ),
		'id' => $prefix . 'alumniemail',
		'type' => 'text',
	));

	$alumni->add_field( array(
		'name' => __( 'Session', 'cmb2' ),
		'id' => $prefix . 'alumnisession',
		'type' => 'text',
	));

	/* $alumni->add_field( array(
		'name'       => __( 'Image', 'wds-post-submit' ),
		'id'         => 'submitted_post_thumbnail',
		'type'       => 'text',
		'attributes' => array(
			'type' => 'file', // Let's use a standard file upload field
		),
	) ); */

	$alumni->add_field( array(
		'name' => __( 'current Job Position', 'cmb2' ),
		'id' => $prefix . 'alumnijobposition',
		'type' => 'text',
	));

}


add_action( 'cmb2_init', 'book_add_metabox' );

function book_add_metabox(){

	$prefix = '_cmb_';

	$alumni = new_cmb2_box( array(
		'id'           => $prefix .'book',
		'title'        => __( 'Book Meta', 'cmb2' ),
		'object_types' => array( 'book' ),
		'context'      => 'normal',
		'priority'     => 'default',
	) );

	$alumni->add_field( array(
		'name' => __( 'Book Name', 'cmb2' ),
		'id' => $prefix . 'bookname',
		'type' => 'text',
	));

	$alumni->add_field( array(
		'name' => __( 'Book Author', 'cmb2' ),
		'id' => $prefix . 'bookauthor',
		'type' => 'text',
	));

	$alumni->add_field( array(
		'name' => __( 'Book ISBN NUMBER', 'cmb2' ),
		'id' => $prefix . 'bookisbn',
		'type' => 'text',
	));

}


add_action( 'cmb2_init', 'exfaculty_add_metabox' );

function exfaculty_add_metabox(){
	
    $prefix = 'cmb2';

	$exexfaculty= new_cmb2_box( array(
		'id'           => $prefix .'cmb2',
		'title'        => __( 'ExFaculty Meta', 'cmb2' ),
		'object_types' => array('xteacher'),
		'context'      => 'normal',
		'priority'     => 'default',
	) );

	$exexfaculty->add_field( array(
		'name' => __( 'Name', 'cmb2' ),
		'id' => $prefix . 'xname',
		'type' => 'text',
	));

	$exexfaculty->add_field( array(
		'name' => __( 'Designation', 'cmb2' ),
		'id' => $prefix . 'xdesignation',
		'type' => 'text',
	));

	$exexfaculty->add_field( array(
		'name' => __( 'Email', 'cmb2' ),
		'id' => $prefix . 'xemail',
		'type' => 'text',
	));

	$exexfaculty->add_field( array(
		'name' => __( 'Phone', 'cmb2' ),
		'id' => $prefix . 'xphone',
		'type' => 'text',
	));

}

add_action( 'cmb2_init', 'course_teacher_add_metabox' );

function course_teacher_add_metabox(){


	$prefix = '_cmb_';

	$course_teacher = new_cmb2_box( array(
		'id'           => $prefix . 'course_teacher_metabox',
		'title'        => __( 'Course Teacher ', 'cmb2' ),
		'object_types' => array( 'coursematerial'),
		'context'      => 'normal',
		'priority'     => 'default',
	));

	$course_teacher->add_field( array(
	    'name'             => 'Course Teacher Name',
	    'id'               => 'course_teacher_name',
	    'type'             => 'select',
	    'show_option_none' => true,
	    'default'          => 'custom',
	    'options_cb' => 'show_course_teacher_options',
	) );

		$course_teacher->add_field( array(
	    'name' => 'Course Material',
	    'desc' => '',
	    'id'   => 'course_material_file',
	    'type' => 'file_list',
	    // 'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
	    // Optional, override default text strings
	    'text' => array(
	        'add_upload_files_text' => 'Replacement', // default: "Add or Upload Files"
	        'remove_image_text' => 'Replacement', // default: "Remove Image"
	        'file_text' => 'Replacement', // default: "File:"
	        'file_download_text' => 'Replacement', // default: "Download"
	        'remove_text' => 'Replacement', // default: "Remove"
	    ),
	) );

}

function show_course_teacher_options(){
	
	$teacherargs = array( 'posts_per_page' =>-1,'post_type' => 'teacher');
	
	$teacher = get_posts( $teacherargs );
	
	$school_contact = array();

    foreach($teacher as $post) : setup_postdata( $post );
			
		$school_contact[$post->ID]=$post->post_title;
	
	endforeach; 
			
	wp_reset_postdata();

	return $school_contact;
}


function show_research_options(){
	
	$teacherargs = array( 'posts_per_page' =>-1,'post_type' => 'teacher');
	
	$teacher = get_posts( $teacherargs );
	
	$school_contact = array();

    foreach($teacher as $post) : setup_postdata( $post );
			
		$school_contact[$post->ID]=$post->post_title;
	
	endforeach; 
			
	wp_reset_postdata();

	return $school_contact;
}
?>


<?php


function wds_frontend_cmb2_get($metabox_id,$object_id) {
	//$metabox_id = 'front-end-post-form';
	//$object_id  = 'post';
	return cmb2_get_metabox( $metabox_id, $object_id );
}


function wds_do_frontend_form_submission_shortcode( $atts = array() ) {

	$cmb = wds_frontend_cmb2_get('_cmb_Alumni','Alumni');
	$post_types = $cmb->prop( 'object_types' );
	$user_id = get_current_user_id();
	$atts = shortcode_atts( array(
		'post_author' => $user_id ? $user_id : 1, // Current user, or admin
		'post_status' => 'pending',
		'post_type'   => reset( $post_types ), // Only use first object_type in array
	), $atts, 'cmb_frontend_alumni_form' );

	foreach ( $atts as $key => $value ) {
		$cmb->add_hidden_field( array(
			'field_args'  => array(
				'id'    => "atts[$key]",
				'type'  => 'hidden',
				'default' => $value,
			),
		) );
	}

	$output = '';

	if ( ( $error = $cmb->prop( 'submission_error' ) ) && is_wp_error( $error ) ) {
		// If there was an error with the submission, add it to our ouput.
		$output .= '<h3>' . sprintf( __( 'There was an error in the submission: %s', 'wds-post-submit' ), '<strong>'. $error->get_error_message() .'</strong>' ) . '</h3>';
	}
	// If the post was submitted successfully, notify the user.
	if ( isset( $_GET['post_submitted'] ) && ( $post = get_post( absint( $_GET['post_submitted'] ) ) ) ) {
		// Get submitter's name
		$name = get_post_meta( $post->ID, 'submitted_author_name', 1 );
		$name = $name ? ' '. $name : '';
		// Add notice of submission to our output
		$output .= '<h3>' . sprintf( __( 'Thank you%s, your new post has been submitted and is pending review by a site administrator.', 'wds-post-submit' ), esc_html( $name ) ) . '</h3>';
	}
	// Get our form
	$output .= cmb2_get_metabox_form( $cmb, 'Alumni', array( 'save_button' => __( 'Submit Post', 'wds-post-submit' ) ) );
	
	return $output;
}

add_shortcode( 'cmb_frontend_alumni_form', 'wds_do_frontend_form_submission_shortcode' );



function wds_handle_frontend_new_post_form_submission() {
	// If no form submission, bail
	if ( empty( $_POST ) || ! isset( $_POST['submit-cmb'], $_POST['object_id'] ) ) {
		return false;
	}
	// Get CMB2 metabox object
	$cmb = wds_frontend_cmb2_get('_cmb_Alumni','Alumni');
	$post_data = array();
	// Get our shortcode attributes and set them as our initial post_data args
	if ( isset( $_POST['atts'] ) ) {
		foreach ( (array) $_POST['atts'] as $key => $value ) {
			$post_data[ $key ] = sanitize_text_field( $value );
		}
		unset( $_POST['atts'] );
	}
	// Check security nonce
	if ( ! isset( $_POST[ $cmb->nonce() ] ) || ! wp_verify_nonce( $_POST[ $cmb->nonce() ], $cmb->nonce() ) ) {
		return $cmb->prop( 'submission_error', new WP_Error( 'security_fail', __( 'Security check failed.' ) ) );
	}
	// Check title submitted
	if ( empty( $_POST['_cmb_alumniname'] ) ) {
		return $cmb->prop( 'submission_error', new WP_Error( 'post_data_missing', __( 'New post requires a alumni Name.' ) ) );
	}
	// And that the title is not the default title
	if ( $cmb->get_field( '_cmb_alumniname' )->default() == $_POST['_cmb_alumniname'] ) {
		return $cmb->prop( 'submission_error', new WP_Error( 'post_data_missing', __( 'Please enter a new alumniname Name.' ) ) );
	}

	/*_cmb_bookname
	_cmb_bookauthor
	_cmb_bookisbn*/
	/**
	 * Fetch sanitized values
	 */
	$sanitized_values = $cmb->get_sanitized_values( $_POST );
	// Set our post data arguments
	$post_data['post_title']   = $sanitized_values['_cmb_alumniname'];
	unset( $sanitized_values['submitted_post_title'] );
	/*$post_data['post_content'] = $sanitized_values['submitted_post_content'];
	unset( $sanitized_values['submitted_post_content'] );*/
	// Create the new post
	$new_submission_id = wp_insert_post( $post_data, true );
	// If we hit a snag, update the user
	if ( is_wp_error( $new_submission_id ) ) {
		return $cmb->prop( 'submission_error', $new_submission_id );
	}
	/**
	 * Other than post_type and post_status, we want
	 * our uploaded attachment post to have the same post-data
	 */
	unset( $post_data['post_type'] );
	unset( $post_data['post_status'] );
	// Try to upload the featured image
	$img_id = wds_frontend_form_photo_upload( $new_submission_id, $post_data );
	//If our photo upload was successful, set the featured image
	if ( $img_id && ! is_wp_error( $img_id ) ) {
		set_post_thumbnail( $new_submission_id, $img_id );
	}
	// Loop through remaining (sanitized) data, and save to post-meta
	foreach ( $sanitized_values as $key => $value ) {
		if ( is_array( $value ) ) {
			$value = array_filter( $value );
			if( ! empty( $value ) ) {
				update_post_meta( $new_submission_id, $key, $value );
			}
		} else {
			update_post_meta( $new_submission_id, $key, $value );
		}
	}
	/*
	 * Redirect back to the form page with a query variable with the new post ID.
	 * This will help double-submissions with browser refreshes
	 */
	wp_redirect( esc_url_raw( add_query_arg( 'post_submitted', $new_submission_id ) ) );
	exit;
}
add_action( 'cmb2_after_init', 'wds_handle_frontend_new_post_form_submission' );
/**
 * Handles uploading a file to a WordPress post
 *
 * @param  int   $post_id              Post ID to upload the photo to
 * @param  array $attachment_post_data Attachement post-data array
 */
function wds_frontend_form_photo_upload( $post_id, $attachment_post_data = array() ) {
	// Make sure the right files were submitted
	if (
		empty( $_FILES )
		|| ! isset( $_FILES['submitted_post_thumbnail'] )
		|| isset( $_FILES['submitted_post_thumbnail']['error'] ) && 0 !== $_FILES['submitted_post_thumbnail']['error']
	) {
		return;
	}
	// Filter out empty array values
	$files = array_filter( $_FILES['submitted_post_thumbnail'] );
	// Make sure files were submitted at all
	if ( empty( $files ) ) {
		return;
	}
	// Make sure to include the WordPress media uploader API if it's not (front-end)
	if ( ! function_exists( 'media_handle_upload' ) ) {
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
		require_once( ABSPATH . 'wp-admin/includes/media.php' );
	}
	// Upload the file and send back the attachment post ID
	return media_handle_upload( 'submitted_post_thumbnail', $post_id, $attachment_post_data );
}


function cmb2_output_file_list( $file_list_meta_key, $img_size = 'medium' ) {

    // Get the list of files
    $files = get_post_meta( get_the_ID(), $file_list_meta_key, 1 );

    echo '<div class="file-list-wrap">';

    foreach((array) $files as $attachment_id => $attachment_url ) {
        echo '<div class="file-list-image">';
        echo wp_get_attachment_image( $attachment_id, $img_size );
        echo '</div>';
    }

    echo '</div>';
}


