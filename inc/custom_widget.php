<?php

class Research extends WP_Widget {

    function Research() {
        $widget_ops = array( 'classname' => 'Research_widget', 'description' => 'Description' );
        $this->WP_Widget('Research_widget', 'Research Widget', $widget_ops );
    }

    /*function __construct() {
			$widget_ops = array( 'classname'   => 'bestseller_products-widget', 'description' => __( 'List bestseller products of your site with thumbnails' ) );
			$this->WP_Widget( 'bestseller_products-widget', __( 'bestseller products' ), $widget_ops );
	}*/

    function widget( $args, $instance ) {
        extract( $args, EXTR_SKIP );

        $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Research', '' ) : esc_html( $instance['title'] )  );
		$rid=get_queried_object_id();
		
        if( is_page_template('teacher.php') || is_page_template('taxonomy-speciality.php') || is_page_template('publication.php') || is_page_template('research.php') || 
        	is_singular( array( 'publication','research')) || get_queried_object_id()=='32' || get_queried_object_id()=='585' || get_queried_object_id()=='538') {

        	//echo get_queried_object_id();

        	//$rid=get_queried_object_id();

        	//if ( is_singular() and 'Research' === get_post_type()){
							
				
			//}
?>
<style>

	#WR<?php echo $rid;?>{
		display: block !important;
	}


</style>

 <?php

		        echo $before_widget;
		        echo $before_title;
		        	echo $title; // Can set this with a widget option, or omit altogether
		        echo $after_title;
		        	$args = array('post_type' => 'Research','posts_per_page' => 12);
					$teacher= new WP_Query($args);
					if($teacher->have_posts()):
						while($teacher->have_posts() ) : $teacher->the_post(); 
						//$per=the_permalink();
						global $post;
						//if(is_single('Research')){
						
							
						echo "<ul   class=\"rsg\">"; 
						echo "<li> <a href=". get_permalink() .">" . get_the_title() . "</a>
							<a class=\"right_angle\"> <i class=\"fa fa-angle-down\"></i> </a>";

							$post_id=$post->ID;

						echo "<ul id=\"WR$post_id\" class=\"rsglist\">";
									
 ?>
									
									<li> <a href="<?php  echo get_permalink( get_page_by_path( 'research-member'));?>
									?post_type=Research&post_id=<?php echo $post_id; ?>&sm=member"> Member</a> </li>
						
	<li> <a href="<?php  echo get_permalink( get_page_by_path( 'research-member'));?>
									?post_type=Research&post_id=<?php echo $post_id; ?>&pub
=publication"> Publication </a> </li>
<li> <a href=" <?php  the_permalink(); ?>"> talks </a> </li> 
									
									<!-- <li> <a href="<?php  the_permalink(); ?>#member" class="wmember" >  Member <?php get_permalink(585); ?> </a> </li>
									
									<li> <a href=" <?php  the_permalink(); ?>#publication" class="wpublication"> Publication </a> </li>
									<li> <a href=" <?php  the_permalink(); ?>"> Talks </a> </li>  -->

<?php
									echo "</ul>";

									echo"</li>";

							echo '</ul>';

						

						endwhile; 
						endif;  
					echo $after_widget;
		}
	
    }

    function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
        $instance['title'] = sanitize_text_field( $new_instance['title'] );
        return $instance ;
    }

    function form( $instance ) { ?>
       	    	
   	<p>
		<label for="<?php echo $this->get_field_id( "title" ); ?>"><?php _e( 'Title' ); ?>: </label>
		<input class="widefat" id="<?php echo $this->get_field_id( "title" ); ?>" 
			   name="<?php echo $this->get_field_name( "title" ); ?>" type="text" 
		       value="<?php echo esc_attr( $instance["title"] ); ?>" />
		
	</p>

   <?php }
}

add_action( 'widgets_init','custom_widget_register');

function custom_widget_register(){
	register_widget( 'Research');
}


add_action( 'wp_enqueue_scripts', 'education_hub_widget_scripts' );

function education_hub_widget_scripts(){
	wp_enqueue_script('research_widget_js', get_template_directory_uri() .'/js/Research_widget.js');
}