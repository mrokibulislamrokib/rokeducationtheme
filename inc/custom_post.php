<?php

class post_type{
	
	public function __construct($name, $singluar_name, $args){
		$this -> register_post_type($name, $singluar_name, $args);	
	}	
	
	//Registering Post Types
	public function register_post_type($name, $singluar_name, $args){ 
		  
	    $args = array_merge(
			array(
				'labels' => array(
							'name' 			=> $name,
							'singular_name' => $singluar_name,
							'add_new'		=> "Add New $singluar_name",
							'add_new_item' 	=> "Add New $singluar_name",
							'edit_item' 	=> "Edit $singluar_name",
							'new_item' 		=> "New $singluar_name",
							'view_item' 	=> "View $singluar_name",
							'search_items' 	=> "Search $name",
							'not_found' 	=> "No $name found",
							'all_items' => "All $name",
							'not_found_in_trash' 	=> "No $name found in Trash", 
							'parent_item_colon' 	=> '',
							'menu_name' 	=>  $name
						  ),
				'public' 	=> true,
				'query_var' => strtolower($singluar_name),
				'hierarchical' => true,
				'rewrite' 	=> array('slug' => $name),
				'menu_icon' =>	admin_url().'images/media-button-video.gif',		 
				'supports' 	=> array('title','editor')
			),
			$args  
	    );
		  
		register_post_type(strtolower($name),$args);
	}
	
	//Taxonomies
	public function taxonomies($post_types, $tax_arr){		
		
		$taxonomies = array();

		foreach ($tax_arr as $name => $arr){

			$singular_name = $arr['singular_name'];

			$labels = array(
					'name' => $name,
					'singular_name' => $singular_name,
					'add_new' => "Add New $singular_name",
					'add_new_item' => "Add New $singular_name",
					'edit_item' => "Edit $singular_name",
					'new_item' => "New $singular_name",
					'view_item' => "View $singular_name",
					'update_item' => "Update $singular_name",
					'search_items' => "Search $name",
					'not_found' => "$name Not Found",
					'not_found_trash' => "$name Not Found in Trash",
					'all_items' => "All $name",
					'separate_items_with_comments' => "Separate tags with commas"
			);

			$defaultArr = array(
				'hierarchical' => true,
				'query_var' => true,
				'rewrite' => array('slug' => $name),
				'labels' => $labels	
			);

			$taxonomies[$name] =  array_merge($defaultArr, $arr);	
		}
		
		$this -> register_all_taxonomies($post_types, $taxonomies);	
	}
	
	public function register_all_taxonomies($post_types, $taxonomies){	
		foreach($taxonomies as $name => $arr){
			register_taxonomy(strtolower($name),strtolower($post_types), $arr);
		}
	}
}


function init_post_type(){
    $course_material_arr = array('supports' => array('title','editor'),);
    $course_material = new post_type('coursematerial', 'coursematerial', $course_material_arr);
    $books_arr = array('supports' => array('title','editor','thumbnail'),);
    $book= new post_type('Book', 'book', $books_arr );
    $alumni_arr = array('supports' => array('title','editor','thumbnail'),);
    $alumni= new post_type('Alumni ', 'alumni', $alumni_arr ); 
    $gallery_arr = array('supports' => array('title','editor','thumbnail'),);
    $gallery_tax_arr =array("gallerycategories"   => array('singular_name' => 'gallery Category','query_var'=> true)); 
    $gallery = new post_type('Gallery', 'gallery', $gallery_arr);
    $gallery->taxonomies('Gallery', $gallery_tax_arr);
    $event_arr = array('supports' => array('title','editor','thumbnail'),);
    //$event_tax_arr =array("eventcategories"   => array('singular_name' => 'event Category','query_var'=> true)); 
    $event = new post_type('event', 'event', $event_arr);
    //$event->taxonomies('event', $event_tax_arr);
    $news_arr = array('supports' => array('title','editor','thumbnail'),);
    //$news_tax_arr =array("newscategories"   => array('singular_name' => 'news Category','query_var'=> true)); 
    $news = new post_type('news', 'news', $news_arr);
    //$news->taxonomies('news', $news_tax_arr);
    $teacher_arr = array('supports' => array('title','thumbnail'),);
    $teacher_speciality_arr =array("speciality"   => array('singular_name' => 'speciality','query_var'=> true));
    $teacher_ex_faculty_member_arr =array("exfaculty_member"   => array('singular_name' => 'exfacultymember','query_var'=> true));
    $teacher_research_arr =array("research"   => array('singular_name' => 'research','query_var'=> true)); 
    $teacher = new post_type('Teacher', 'teacher', $teacher_arr);
    $teacher->taxonomies('Teacher', $teacher_speciality_arr);
    $teacher->taxonomies('Teacher', $teacher_ex_faculty_member_arr );
     $ex_arr = array('supports' => array('title','thumbnail'),);
     $ex= new post_type('xteacher', 'xteacher', $ex_arr );

    $student_arr = array('supports' => array('thumbnail'),);
    $student_tax_arr =array("studentcategories"   => array('singular_name' => 'Student Category','query_var'=> true)); 
    $student = new post_type('Student', 'student', $student_arr);
    $student->taxonomies('Student', $student_tax_arr);
    
 	$Staff_arr = array('supports' => array('thumbnail'),);
    $Staff_tax_arr =array("staffcategories"   => array('singular_name' => 'Staff Category','query_var'=> true)); 
    $Staff = new post_type('Staff', 'Staff', $Staff_arr );
    $Staff->taxonomies('Staff', $Staff_tax_arr);
    
    $research_arr = array('supports' => array('title','editor','thumbnail'),);
	$research = new post_type('Research','research', $research_arr);

	$publication_arr = array('supports'=>array('thumbnail','title'),);
	$publication = new post_type('Publication','publication', $publication_arr);
}

add_action('init', 'init_post_type');


add_filter( 'manage_teacher_posts_columns', 'set_custom_edit_teacher_columns' );
add_action( 'manage_teacher_posts_custom_column' , 'custom_teacher_column', 10, 2 );

function set_custom_edit_teacher_columns($columns) {
    //unset( $columns['title'] );
    $columns['teacher_name'] = __( 'Name', '' );
    $columns['teacher_designation'] = __( 'Designation', '' );
    return $columns;
}

function custom_Teacher_column( $column, $post_id ) {
    switch ( $column ) {

        case 'teacher_name' :
        	 echo get_post_meta( $post_id ,'_cmb_name' ,true ); 
        	 break;
        case 'teacher_designation' :
           echo get_post_meta( $post_id , '_cmb_designation' , true ); 
            break;

    }
}


add_filter( 'manage_publication_posts_columns', 'set_custom_edit_publication_columns' );
add_action( 'manage_publication_posts_custom_column' , 'custom_publication_column', 10, 2 );

function set_custom_edit_publication_columns($columns) {
    //unset($columns['title'] );
    $columns['paper_name'] = __( 'Paper Name', '' );
    return $columns;
}

function custom_publication_column( $column, $post_id ) {
    switch ( $column ) {

        case 'paper_name' :
        	echo get_post_meta( $post_id ,'_cmb_papername' ,true ); 
        	break;
    }
}

add_filter( 'manage_staff_posts_columns', 'set_custom_edit_staff_columns' );
add_action( 'manage_staff_posts_custom_column' , 'custom_staff_column', 10, 2 );

function set_custom_edit_staff_columns($columns) {
    unset( $columns['title'] );
    $columns['staff_name'] = __( 'Name', '' );
    return $columns;
}

function custom_staff_column( $column, $post_id ) {
    switch ( $column ) {

        case 'staff_name' :
        	 echo get_post_meta( $post_id ,'_cmb_staffname' ,true ); 
        	 break;
    }
}

add_action( 'admin_init', 'add_meta_boxes' );

function add_meta_boxes() {
    add_meta_box('publication_teacher_metabox','Publication Teacher','publication_teacher_field','publication');
    add_meta_box('research_teacher_metabox','Research Teacher','research_teacher_field','research');
    add_meta_box('research_publication_metabox','Research Publication','research_publication_field','research');
    add_meta_box('teacher_publication_metabox',' Teacher Publication','teacher_publication_field','teacher');
}

function teacher_publication_field(){
		global $post;
		
		$selected_teacher_pub=get_post_meta($post->ID,'teachers_pub_meta',true);
		
		$selected_teacher_research=get_post_meta($post->ID,'teacher_research_meta',true);

		$publicationargs = array( 'posts_per_page' =>-1,'post_type' => 'publication');
		$publication = get_posts( $publicationargs );
		
		$researchargs = array( 'posts_per_page' =>-1,'post_type' => 'research');
		
		$research = get_posts( $researchargs );

		print_r($selected_teacher_pub);
		
		print_r($selected_teacher_research);

?>


    	<select name="publication_meta[]"  multiple>

		<?php foreach ( $publication as $post ) : setup_postdata( $post );
				
			$papername=get_post_meta($post->ID,'_cmb_papername','true'); ?>
			
			<option value="<?php echo $post->ID; ?>" <?php // 
				if(isset($selected_teacher_pub) && !empty($selected_teacher_pub)){
				 	if(in_array( $post->ID, $selected_teacher_pub )){ echo 'selected="selected"';}
				 	else{ echo '';}
				} ?> > <?php echo $papername; ?> 
			</option>
		
		<?php 
			endforeach; 
			wp_reset_postdata();
		?>
    	
    	</select>

		
		<select name="teacher_research_meta[]"  multiple>

		<?php foreach ($research as $post ) : setup_postdata( $post );
				
			//$papername=get_post_meta($post->ID,'_cmb_papername','true'); ?>
			
			<option value="<?php echo $post->ID; ?>" 
				
				<?php  

					if(isset($selected_teacher_research) && !empty($selected_teacher_research)){
						if(in_array( $post->ID, $selected_teacher_research )){ 
							echo 'selected="selected"';
						}else{ 
							echo '';
						}
					} 
				?> > <?php echo $post->post_title; ?>  </option>
		
		<?php 
			endforeach; 
			wp_reset_postdata();
		?>
    	
    	</select>



<?php }


function research_publication_field(){

		global $post;
		

?>


<?php }


function research_teacher_field(){
	   
	    global $post;
	    $selected_research_teacher=get_post_meta($post->ID,'research__teacher_meta',true);
	 //  print_r($selected_research_teacher);
		$teacherargs = array( 'posts_per_page' =>-1,'post_type' => 'teacher');
		$teacher = get_posts( $teacherargs );
		/*$publicationargs = array( 'posts_per_page' =>-1,'post_type' => 'publication');
		$publication = get_posts( $publicationargs );*/

		$selected_research_pub=get_post_meta($post->ID,'research__pub__meta',true);
	
		$publicationargs = array( 'posts_per_page' =>-1,'post_type' => 'publication');
		$publication = get_posts( $publicationargs );
?>

		<select name="research__teacher_meta[]"  multiple>

		<?php foreach ( $teacher as $post ) : setup_postdata( $post );
				get_post_meta( $post_id, $key, $single); ?>
			
			<option value="<?php echo $post->ID; ?>"  <?php

			if(isset($selected_research_teacher) && !empty($selected_research_teacher)){

			   if(in_array( $post->ID, $selected_research_teacher )){ echo 'selected="selected"';} else{ echo '';} } ?> > <?php echo $post->post_title; ?> </option>
		
		<?php 
			endforeach; 
			wp_reset_postdata();
		?>
    	
    	</select>



		<select name="research__pub_meta[]"  multiple>
		
				<?php foreach ( $publication as $post ) : setup_postdata( $post );
				
			$papername=get_post_meta($post->ID,'_cmb_papername','true'); 


			print_r($selected_research_pub); ?>
			
			<option value="<?php echo $post->ID; ?>" <?php  

			if(isset($selected_research_pub) && !empty($selected_research_pub)){
				if(in_array( $post->ID, $selected_research_pub )){ echo 'selected="selected"';}else{ echo '';} }?> > <?php echo $papername; ?> </option>
		
		<?php 
			endforeach; 
			wp_reset_postdata();
		?>
    	</select>

<?php

}


function publication_teacher_field() {

    	global $post;
    	
    	$selected_pub_teacher= get_post_meta( $post->ID, 'pub_teachers_meta', true );

    	print_r($selected_pub_teacher);

		$args = array( 'posts_per_page' =>-1,'post_type' => 'teacher');
		
		$myposts = get_posts( $args );

		?>

		<select name="teachers_meta[]"  multiple>

		<?php foreach ( $myposts as $post ) : setup_postdata( $post );

				$name=get_post_meta($post->ID,'_cmb_name',true);

		 ?>
		 if()
			
			<option value="<?php echo $post->ID; ?>" <?php

			if(isset($selected_pub_teacher) && !empty($selected_pub_teacher)){
			  if(in_array( $post->ID, $selected_pub_teacher )){ echo 'selected="selected"';}else{ echo '';} } ?> >
				 <?php  //echo $post->post_title;
				 		echo $name;
				 ?> 
			</option>
		
		<?php 
			endforeach; 
			wp_reset_postdata();
		?>
    	
    	</select>

<?php  }




add_action( 'save_post', 'save_publication_field',10,2);
add_action( 'save_post', 'save_teacher_field',10,2);
add_action( 'save_post', 'save_research_field',10,2);

function save_publication_field($post_id, $post) {

	global $post;
    if ('publication'!= $post->post_type) 
        return ;        

    // verify nonce
   /* if ( empty( $_POST['publication_nonce'] ) || !wp_verify_nonce( $_POST['publication_nonce'], basename( __FILE__ ) ) )
        return $post_id;*/

    // check autosave
   /* if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return $post_id;
*/
    // check permissions
   /* if ( !current_user_can( 'edit_post', $post_id ) ) {
        return $post_id;*/
  		

  		$values=$_POST['teachers_meta'];

  		$pub_teacher = array();

  		foreach ($values as $key => $value) {
  			$pub_teacher["$key"]=$value;
  		}


  		update_post_meta($post->ID, 'pub_teachers_meta', $pub_teacher);

  		//update_post_meta( $post_id, 'pub_teachers_meta', array_map( 'intval',) );
}


function save_book_meta( $post_id, $post, $update ) {


    // - Update the post's metadata.

    if ( isset( $_REQUEST['book_author'] ) ) {
        update_post_meta( $post_id, 'book_author', sanitize_text_field( $_REQUEST['book_author'] ) );
    }

    if ( isset( $_REQUEST['publisher'] ) ) {
        update_post_meta( $post_id, 'publisher', sanitize_text_field( $_REQUEST['publisher'] ) );
    }

    // Checkboxes are present if checked, absent if not.
    if ( isset( $_REQUEST['inprint'] ) ) {
        update_post_meta( $post_id, 'inprint', TRUE );
    } else {
        update_post_meta( $post_id, 'inprint', FALSE );
    }
}
add_action( 'save_post', 'save_book_meta', 10, 3 );

function save_teacher_field($post_id, $post) {

		global $post;
    	
    	if('teacher'!= $post->post_type) 
        	return ;        

  		$teacher_pub_meta=$_POST['publication_meta'];

  		$teacher_research_meta=$_POST['teacher_research_meta'];

  		$teacher_pub = array();

  		$teacher_research = array();

  		foreach ($teacher_pub_meta as $key => $value) {
  			$teacher_pub["$key"]=$value;
  		}

  		foreach ($teacher_research_meta as $key => $value) {
  			$teacher_research["$key"]=$value;
  		}

  		update_post_meta($post->ID, 'teachers_pub_meta', $teacher_pub);

  		update_post_meta($post->ID, 'teacher_research_meta', $teacher_research);


}

function save_research_field($post_id, $post){

		global $post;
    	
    	if('research'!= $post->post_type) 
        	return ;        

  		$research_teacher_meta=$_POST['research__teacher_meta'];

  		$research_publication_meta=$_POST['research__pub_meta'];

  		$research_teacher=array();

  		$research_pub = array();

  		foreach($research_teacher_meta as $key => $value) {
  			
  				$research_teacher["$key"]=$value;
  		}

  		foreach($research_publication_meta as $key => $value) {
  			
  				$research_pub["$key"]=$value;
  		}

  		update_post_meta($post->ID, 'research__teacher_meta', $research_teacher);

  		update_post_meta($post->ID, 'research__pub__meta', $research_pub);
}