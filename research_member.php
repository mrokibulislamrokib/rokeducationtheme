<?php /* Template Name: Researh Member Template */ get_header(); ?>
	
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="research-page-content">	
	
		<?php
			if(isset($_GET['post_type']) && isset($_GET['post_id']) && isset($_GET['sm'])){
				$post_id=$_GET['post_id'];
				$post_type=$_GET['post_type'];
				$research__teacher_core[]=array();
				$research__teacher_asso[]=array();
				$entries = get_post_meta($post_id,'research_teacher_group', true );
			?>

			<style>

				#WR<?php echo $post_id;?>{
					display: block !important;
				}


			</style>
			
			<div class="Research_member" id="member">

				<h3 > Researh Member </h3>

			<?php

				foreach ( (array) $entries as $key => $entry ) {
				    $name = $type='';
				    if (isset($entry['research_member_name'] ) )
				        $name =$entry['research_member_name'];
				    if ( isset( $entry['research_member_type'] ) )
				        $type =$entry['research_member_type'];

				   if($type==='Core'){
				   		$name=(int)$name;
				    	//array_push($research__teacher_core,$name);
				    	$research__teacher_core[]=$name;
				    }

				    if($type==='Associate'){
				    	$name=(int)$name;
				    	//array_push($research__teacher_asso,$name);
				    	$research__teacher_asso[]=$name;
				    }				

				} ?>
			</div>

			

				

			<div class="Research_member" id="member">
				

				<?php
					
					//$research__teacher_core=array_unshift($research__teacher_core);
					unset($research__teacher_core[0]);

					if(!empty($research__teacher_core)){ ?>

						<h3 > Core Member </h3>

				<?php   foreach($research__teacher_core as $key => $value) {		
		    				$series = new WP_Query(array('post_type' => 'teacher','p' =>$value,'nopaging' => true));
							if($series->have_posts()){ 
								while ( $series->have_posts() ) { $series->the_post(); 
									$name= get_post_meta(get_the_ID(), '_cmb_name', true); 
								    $designation= get_post_meta($post->ID, '_cmb_designation', true); 
									require( locate_template( 'template-parts/content-teacher.php'));
								} 
							}
						}

					}else{
						_e('There is no member in research');
					}
				?>

			</div>



			<div class="Research_member" id="member">
				

				<?php
					
					//$research__teacher_core=array_unshift($research__teacher_core);
					unset($research__teacher_asso[0]);

					if(!empty($research__teacher_asso)){ ?>

						<h3 > Associate Member </h3>

				<?php   foreach($research__teacher_asso as $key => $value) {		
		    				$series = new WP_Query(array('post_type' => 'teacher','p' =>$value,'nopaging' => true));
							if($series->have_posts()){ 
								while ( $series->have_posts() ) { $series->the_post(); 
									$name= get_post_meta(get_the_ID(), '_cmb_name', true); 
								    $designation= get_post_meta($post->ID, '_cmb_designation', true); 
									require( locate_template( 'template-parts/content-teacher.php'));
								} 
							}
						}

					}else{
						_e('There is no member in research');
					}
				?>

			</div>

			


			<?php //var_dump($research__teacher_core); var_dump($research__teacher_asso);  
		}

			if(isset($_GET['post_type']) && isset($_GET['post_id']) && isset($_GET['pub'])){
				$post_id=$_GET['post_id'];
				$post_type=$_GET['post_type'];
				$research__pub=get_post_meta($post_id, 'research__pub__meta',true); ?>

				<style>

				#WR<?php echo $post_id;?>{
					display: block !important;
				}


			</style>

				<div class="publication_research" id="publication">

					<h3>Publication</h3>
					
					<ul>
							
					<?php

						if(!empty($research__pub)){

							foreach ($research__pub as $key => $value) {
														    			
								$series = new WP_Query( array(
								    'post_type' => 'publication',
								    'p' =>$value,
								    'nopaging' => true
								));

								if($series-> have_posts()){ 
									while ( $series->have_posts() ) { 
										$series->the_post(); 
										$pub_name= get_post_meta(get_the_ID(), '_cmb_papername', true); 

						?>
																			    	
									<li><a href="<?php the_permalink(); ?>"> <?php echo $pub_name; ?> </a></li>
																			    

						<?php 	
										} 
									
									}

								}

							}else{
								_e('There is no member in research');
							}
						?>
						</ul>
						
					</div>
			

			<?php } ?>

		</div>
	</main><!-- #main -->
</div><!-- #primary -->
<?php do_action( 'education_hub_action_sidebar' ); ?>

<?php get_footer(); ?>
