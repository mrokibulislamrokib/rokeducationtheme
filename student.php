<?php 
	/* Template Name: student Template */
get_header(); ?>
	
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php 
				
				$args = array('post_type' => 'student','posts_per_page' => 12);
				$student= new WP_Query($args);
				if($student->have_posts()):
					while($student->have_posts() ) : $student->the_post(); ?>
						<a href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a>	
			<?php		endwhile; // End of the loop. 
				endif; 
			?>
                        <?php get_sidebar(); ?>

		</main><!-- #main -->
	</div><!-- #primary -->

	<?php do_action( 'education_hub_action_sidebar' ); ?>

<?php get_footer(); ?>
