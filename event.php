<?php 
	/* Template Name: Event Template */
get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			
			<?php
				  $paged=(get_query_var('paged')) ? get_query_var('paged') : 1;
				 
				  $query_args = array(
				    'post_type' => 'event',
				    'posts_per_page' =>6,
				    'paged' => $paged
				  );

				  $the_query = new WP_Query( $query_args );
			?>

			<?php 
				if($the_query->have_posts()) : 
					while($the_query->have_posts()) : $the_query->the_post(); 
					global $post; ?>
				
					<div class="event-post">
						<a href="<?php the_permalink(); ?>" >
							<?php 	if ( has_post_thumbnail() ) :
					 				 the_post_thumbnail('event_thumb_image',array('class'=>'alignleft')); 
					 		endif; ?>
					 	</a>
				  		<h3><a href="<?php the_permalink();?>"> <?php the_title(); ?> </a> </h3>
					  	<p><?php // the_excerpt();?></p>


<?php

    $content = get_the_content();
    $trimmed_content = wp_trim_words( $content, 20, NULL );
    echo $trimmed_content;

?>
				  	</div>

			<?php endwhile; ?>

			<?php  wp_reset_postdata();  ?>

			<?php if ($the_query->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>

				<div class="clear"></div>

				<?php
					$pagination_args = array(
					  'base'            => get_pagenum_link(1) . '%_%',
					  'format'          => 'page/%#%',
					  'total'           => $numpages,
					  'current'         => $paged,
					  'show_all'        => False,
					  'end_size'        => 1,
					  'mid_size'        => $pagerange,
					  'prev_next'       => True,
					  'prev_text'       => __('&laquo;'),
					  'next_text'       => __('&raquo;'),
					  'type'            => 'plain',
					  'add_args'        => false,
					  'add_fragment'    => ''
					);

					$paginate_links = paginate_links($pagination_args);

					if ($paginate_links) {
					  echo "<nav class='custom-pagination'>";
					    echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
					    echo $paginate_links;
					  echo "</nav>";
					}
			?>

				<div class="pagenavigation">

				    <?php
							if (function_exists("custom_pagination")) {
				     		 	custom_pagination($the_query->max_num_pages,"",$paged);
				      		}
				    ?>
				    
			    </div>

			<?php } ?>

			<?php // wp_reset_query(); ?>

			<?php else: ?>
				<?php _e('Sorry, no posts matched your criteria.'); ?>
			<?php endif; ?>
		</main><!-- #main -->
	</div><!-- #primary -->

	<?php do_action( 'education_hub_action_sidebar' ); ?>

<?php get_footer(); ?>
