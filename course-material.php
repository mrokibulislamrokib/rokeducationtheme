<?php 
	/* Template Name: Course Material Template */
	get_header(); 
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="teacher-page-content">	

			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

			<?php
				  $paged=(get_query_var('paged')) ? get_query_var('paged') : 1;
				 
				  $query_args = array(
				    'post_type' => 'teacher',
				    'posts_per_page' => 12,
				    'paged' => $paged
				  );
				  $the_query = new WP_Query( $query_args );
			?>

			<?php 	
					if($the_query->have_posts()) : 
						while($the_query->have_posts()) : $the_query->the_post(); 

							global $post;
						    $name= get_post_meta(get_the_ID(), '_cmb_name', true); 
						    $designation= get_post_meta($post->ID, '_cmb_designation', true); 
						    $Office= get_post_meta($post->ID, '_cmb_Office', true);
						    $phone= get_post_meta($post->ID, '_cmb_phone', true);
						    $email= get_post_meta($post->ID, '_cmb_email', true);
						    $website= get_post_meta($post->ID, '_cmb_website', true);
						    $education= get_post_meta($post->ID, '_cmb_education', true);
						    $research= get_post_meta($post->ID, '_cmb_research_interests', true);
						    $post_id=$post->ID;
			?>


			<div class="course_material_teacher_by_speciality faculty">
				
				<h3>  <a href="<?php  echo get_permalink( get_page_by_path( 'material_teacher'));?>
									?&teacher_id=<?php echo $post_id; ?>"> <?php echo $name; ?></a></h3>
		    	
		    	<p> <?php echo $designation; ?> </p> 

			</div>

			<?php endwhile; ?>

			<?php  wp_reset_postdata(); ?>

			<?php if ($the_query->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>

				<div class="clear"></div>

				<?php
					
					$pagination_args = array(
					  'base'            => get_pagenum_link(1) . '%_%',
					  'format'          => 'page/%#%',
					  'total'           => $numpages,
					  'current'         => $paged,
					  'show_all'        => False,
					  'end_size'        => 1,
					  'mid_size'        => $pagerange,
					  'prev_next'       => True,
					  'prev_text'       => __('&laquo;'),
					  'next_text'       => __('&raquo;'),
					  'type'            => 'plain',
					  'add_args'        => false,
					  'add_fragment'    => ''
					);

					$paginate_links = paginate_links($pagination_args);

					if ($paginate_links) {
					  echo "<nav class='custom-pagination'>";
					    echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
					    echo $paginate_links;
					  echo "</nav>";
					}
				?>

				<div class="pagenavigation">

				    <?php
						if (function_exists("custom_pagination")) {
				     		 custom_pagination($the_query->max_num_pages,"",$paged);
				      	}
				    ?>
			    </div>

			<?php } ?>

			<?php else: ?>
				<?php _e('Sorry, no posts matched your criteria.'); ?>
			<?php endif; ?>
		
		</div>			
	</main>
</div>

<?php do_action( 'education_hub_action_sidebar' ); ?>

<?php get_footer(); ?>