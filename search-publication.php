<?php get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="pub-page-content">	
			<h1>Custom Search</h1>

			
			<?php if (have_posts()) : ?>
			    
			    <?php while (have_posts()) : the_post(); 
					 global $post;
					 $name= get_post_meta(get_the_ID(), '_cmb_papername', true); 
					 $journalname= get_post_meta($post->ID, '_cmb_journalname', true); 
					 $pubyear= get_post_meta($post->ID, '_cmb_pubyear', true); 
				?>

				<div class="publication content">
		    		
		    		<div class="publication-title">
		    			<h4> <?php echo $name; ?> </h4>
		    			<p> <?php echo $journalname; ?> </p> 
		    			<p> <?php echo $pubyear; ?> </p>

		    			<ul>
							<li></li>

		    			</ul>
		    			<p> <?php echo $author; ?> </p>
		    		</div>
			    	
			    	<ul class="publication">
			    	
			    	<?php
			    			 $series = new WP_Query( array(
								    'post_type' => 'teacher',
								    'post__in' =>get_post_meta($post->ID, 'pub_teachers_meta', true),
								    'nopaging' => true
								 ));

								if ( $series-> have_posts() ) { 
										while ( $series->have_posts() ) { $series->the_post(); 

											 $teacher_name= get_post_meta(get_the_ID(), '_cmb_name', true); 

					?>
								    	
								    		<li><a href="<?php the_permalink(); ?>"> <?php echo $teacher_name; ?> </a></li>
								    

					<?php 	} 
								
						}
					?>

			    	</ul>

		  		</div>

				<?php endwhile; else: ?>

			<?php endif; ?>
		</div>

	</main>

</div>


<?php do_action( 'education_hub_action_sidebar'); ?>

<?php get_footer(); ?>