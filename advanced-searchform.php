<form method="get" id="advanced-searchform" role="search" action="<?php echo esc_url( home_url( '/' ) ); ?>">

    <h3><?php _e( 'Publication  Search', 'textdomain' ); ?></h3>
    
    <input type="hidden" name="search" value="advanced">

    <div class="advance_search_field">
      <label for="name" class=""><?php _e( 'Paper title: ', 'textdomain' ); ?></label>
      <input type="text" value="" placeholder="<?php _e( 'Type the Paper title', 'textdomain' ); ?>" name="name" id="name" />
    </div>

    <div class="advance_search_field">
     
      <label> <?php _e( 'Author','');?></label>

      <select name="model[]" id="model" class="adv-select">
        <option value=""></option>              
        <?php
              $paged=(get_query_var('paged')) ? get_query_var('paged') : 1;
             
              $query_args = array(
                'post_type' => 'teacher',
                'posts_per_page' => -1,
                'paged' => $paged
              );
              $the_query = new WP_Query( $query_args );
        ?>

        <?php   
                if($the_query->have_posts()) : 
                    while($the_query->have_posts()) : $the_query->the_post(); 
                        global $post;
                        $teacher_name= get_post_meta(get_the_ID(), '_cmb_name', true); 
        ?>

            <option value="<?php echo $post->ID; ?>"> <?php echo $teacher_name; ?> </option>

        <?php endwhile; ?>

        <?php else: ?>
                <?php _e('Sorry, no posts matched your criteria.'); ?>
        <?php endif; ?>
      </select>

    </div>
    
    <div class="advance_search_field">
      <label> <?php _e( 'Publication year','');?></label>
      <input type="text" name="publication_year" >
    </div>

    <div class="advance_search_field">
       <label for=""> <?php _e( 'Journal Name:','');?> </label>
      <input type="text" name="journal_name" id="journal_name">

    </div>
    
    <div class="advance_search_field">
      <input type="submit" id="searchsubmit" value="Search" />
    </div>

</form>

    


    <?php
        
        $teacher_search_array=array('236');
        global $wpdb;
        $sql="SELECT `post_id` FROM `wp_postmeta` WHERE meta_key='pub_teachers_meta'";
        $pub_teacher=$wpdb->get_results($sql);
        $record_num= count($pub_teacher);
        $publication_teacher_array=array();
        $post_pub=array();
       
        for($i=0;$i<$record_num;$i++){
            
            $publication_teacher_array[$pub_teacher[$i]->post_id]=get_post_meta($pub_teacher[$i]->post_id, 'pub_teachers_meta',true);
            
           //print_r($publication_teacher_array[$pub_teacher[$i]->post_id]);

            if(in_array('236',$publication_teacher_array[$pub_teacher[$i]->post_id])){
                 //echo $pub_teacher[$i]->post_id;
                  $post_pub[$i]=$pub_teacher[$i]->post_id;
            }

        }

        //print_r($post_pub);

 /*       $publication_teacher_args=array(
            'post_type' =>  'publication',
            'post_in'   =>  $post_pub
        );

        $publication_Query=new WP_Query($publication_teacher_args);
        
        if( $publication_Query->have_posts() ) :
            while( $publication_Query->have_posts() ) : 

                $publication_Query->the_post(); 

                the_title();  

        endwhile;
            else :
            _e( 'Sorry, nothing matched your search criteria', 'textdomain' );
        endif;
        
        wp_reset_postdata();*/
        

        //print_r($publication_teacher_array);

        //in_array(needle, haystack)

      $sql= "SELECT `post_id` FROM `wp_postmeta` 
             WHERE  
             meta_key='_cmb_journalname' AND  meta_key='_cmb_pubyear' AND meta_key='_cmb_papername' 
             AND
             meta_value='dhaka' OR meta_value='J. Sci.'";

      $meta_sql="SELECT `post_id` FROM `wp_postmeta` 
         WHERE (`meta_key` ='_cmb_journalname' AND `meta_value` = '') AND
         (`meta_key` ='_cmb_pubyear' AND `meta_value` = '') AND
        (`meta_key` ='_cmb_papername' AND `meta_value` = '')";


            /* $query_args = array('meta_key' => 'pub_teachers_meta');
             $meta_query = new WP_Meta_Query();
             $meta_query->parse_query_vars( $query_args );
             $mq = $meta_query->get_sql(
                'post',
                $wpdb->posts,
                'ID',
                null
             );

             print_r($mq);*/

    ?>


<?php
      //  print_r($model_search_array);


        /********** next Experimant ************/

        /*$exp_name = $_GET['name'] != '' ? $_GET['name'] : '';
        $exp_model = $_GET['model'] != '' ? $_GET['model'] : '';        
        $exp_publication_year = $_GET['publication_year'] != '' ? $_GET['publication_year'] : '';
        $exp_journal_name = $_GET['journal_name'] != '' ? $_GET['journal_name'] : '';

        global $wpdb;
        
        $exmp_sql="
            SELECT $wpdb->posts.* 
            FROM $wpdb->posts, $wpdb->postmeta
            WHERE $wpdb->posts.ID = $wpdb->postmeta.post_id 
            AND meta_key='pub_teachers_meta'";


        $exp_pub_teacher=$wpdb->get_results($exmp_sql,OBJECT);
        $record_num= count($exp_pub_teacher);

        if($record_num){

        }*/

?>