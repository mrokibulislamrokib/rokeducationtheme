<?php get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php if(have_posts()):
					while ( have_posts() ) : the_post(); 	 
					 
					global $post;
				     
				    $name= get_post_meta(get_the_ID(), '_cmb_name', true); 
				    $designation= get_post_meta($post->ID, '_cmb_designation', true); 
				    $Office= get_post_meta($post->ID, '_cmb_Office', true);
				    $phone= get_post_meta($post->ID, '_cmb_phone', true);
				    $email= get_post_meta($post->ID, '_cmb_email', true);
				    $website= get_post_meta($post->ID, '_cmb_website', true);
				    $bio= get_post_meta($post->ID, '_cmb_bio', true);
				    $research= get_post_meta($post->ID, '_cmb_research_interests', true);
				    $publication= get_post_meta($post->ID, '_cmb_publication', true);
				    $education= get_post_meta($post->ID, '_cmb_education', true);


				    $teachers_pub=get_post_meta($post->ID, 'teachers_pub_meta',true);
				    $teachers_research=get_post_meta($post->ID, 'teacher_research_meta',true);
			?>





				<div class="sfteacher">

	<div class="sfgrid2">
		<p class="detailtext">  <?php echo $bio; ?> </p>
	</div>
				
	<div class="sfgridIM1">
		<?php  if (has_post_thumbnail()){ 
			the_post_thumbnail('teacher_small_image'); 
		}?>
	</div>

	<div class="sfgrid1">
		<h4><?php echo $name; ?></h4>
        <h3><?php echo $designation; ?></h3>

        <div class="contactdiv_detail">
            <ul>
                <li>
                    <ul>
                        <li><i class="icon-envelope4"></i></li>
                        <li>Email:  
                        	<?php
                            if(!empty($email)){
								foreach ($email as $em) {
									echo "<a href=\"mailto:$em\">". $em."</a> | ";
								} 
							}  
							?> 
						</li>
                    </ul>
                </li>


                <li>
                    <ul>
                        <li><i class="icon-envelope4"></i></li>
                        <li> Office:</li>
                        <li> <?php echo $Office; ?> </li>
                    </ul>
                </li>

                <li>
                    <ul>
                        <li><i class="icon-envelope4"></i></li>
                        <li>website:</li>
                        <li> <?php echo $website; ?> </li>
                    </ul>
                </li>

                <li>
                    <ul>
                        <li><i class="icon-envelope4"></i></li>
                        <li>phone:</li>
                        <li> <?php echo $phone; ?> </li>
                    </ul>
                </li>

                 <li>
                                
                    <ul>
						<li>Research Group:</li>
						
						<?php
							
							if(!empty($teachers_research)){
								
								foreach ($teachers_research as $key => $value) {

									$gr = new WP_Query( array(
									    'post_type' => 'research',
									    'p' =>$value,
									    'nopaging' => true
									));

									if($gr->have_posts()){ 
										while ($gr->have_posts() ) { 
											$gr->the_post(); ?>
										<li><a href="<?php the_permalink(); ?>" class="paper_name"> <?php the_title(); ?> </a></li>
																    

																    	
										
									<?php   }
									}	
								}
							}
						?>
					</ul>
				</li>
            </ul>
        </div>
		
	</div>
	
	
	
	



</div>

		   <!--     <div class="biography"> 
                    <h2>BIOGRAPHY</h2>
                    <p class="detailtext"> <?php echo $bio; ?> </p>                             
               	</div> -->

				<div class="acadimic_experience"> 
	                <h2>Educational Background</h2>
	                <ul class="timeline">
	                	<?php 
							if(!empty($education)){
								foreach ($education as $edu) {
									echo "<li>". $edu."</li>";
								} 
							}	
						?>
                   	</ul>
                </div>

                <div class="research">
					<h1>Research Interest</h1>
					<p><?php echo  $research;?> </p>
				</div>
		
				<div class="pub">
					<h1>Publication</h1>
					<ul class="timeline">
						<?php 
							if(!empty($publication)){
								foreach ($publication as $pub) {
									echo "<li>". $pub."</li>";
								} 
							}
						?>


						<?php

							if(!empty($teachers_pub)){

								foreach ($teachers_pub as $key => $value) {
									$series = new WP_Query( array(
									    'post_type' => 'publication',
									    'p' =>$value,
									    'nopaging' => true
									));

									if($series-> have_posts()){ 
										while ( $series->have_posts() ) { 
											$series->the_post(); 

											$teacher_name= get_post_meta(get_the_ID(), '_cmb_papername', true);
											$journalname= get_post_meta($post->ID, '_cmb_journalname', true); 
											$pubyear= get_post_meta($post->ID, '_cmb_pubyear', true);
											$tauthor= get_post_meta($post->ID, 'pub_teachers_meta', true);
											$DOI= get_post_meta($post->ID, '_cmb_DOI_NUMBER', true); 

						?>
																			    	
											<li><a href="<?php the_permalink(); ?>" class="paper_name"> <?php echo $teacher_name; ?> </a></li>


											<ul class="tpublication">
            
									            <?php
									                if(!empty($tauthor)){
									                    foreach ($tauthor as $key => $value) {
									                        $series = new WP_Query( array(
									                            'post_type' => 'teacher',
									                            'p' =>$value,
									                            'nopaging' => true
									                        ));
									                        if ( $series-> have_posts() ) { 
									                            while ( $series->have_posts() ) { $series->the_post(); 
									                                $teacher_name= get_post_meta(get_the_ID(), '_cmb_name', true);
									                        $pubyear= get_post_meta($post->ID, '_cmb_pubyear', true);
									                        // $author= get_post_meta($post->ID, '_cmb_author', true);
									                        $tauthor= get_post_meta($post->ID, 'pub_teachers_meta', true);
									                        $DOI= get_post_meta($post->ID, '_cmb_DOI_NUMBER', true);
									                        // $pub_teach=get_post_meta($post->ID, 'pub_teachers_meta', true);
									            

									            ?>
									                                            
									                <li><a href="<?php the_permalink(); ?>"> <?php echo $teacher_name; ?> </a></li>
									                                        

									            <?php   } 
									                            
									                    }

									                }

									            }
									            ?>
 <li> <?php echo $journalname;?>  </li>



        									</ul>
																			    

						<?php 	
										} 
									
									}

								}

							}
						?>

					</ul>
				</div>

		<?php 	endwhile; // End of the loop.
			  endif;
		?> 

		</main><!-- #main -->
	</div><!-- #primary -->

<?php do_action( 'education_hub_action_sidebar' ); ?>

<?php get_footer(); ?>